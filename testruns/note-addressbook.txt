Observations from results of address book.

Add Note:
* About 18 seconds of navigation before you can enter a note.
* About 10 seconds to navigate down to enter a new number.
* There's about 3 seconds at the beginning of every "edit."
* Each  input method seems to have its own time to run.
    Created an I operator, which contains both the "composing" bit and the "commit"
    Entering numbers is very fast in general, around .150-.200 seconds to type
    each key (in line with an "average skilled typist" on the keyboard).

    The T9 input method, is a bit slower, .364 to type with that, but there's
    caveats (committing takes a while, when the word differs significantly,
    there's some doubt. Seems to be some sort of "slow start" between
    committing one word and starting another.

    The non-T9 input method is much slower: the average time to hit a key is
    about .229, however, one needs to cycle and there's a need for "waiting"
    about .7 seconds to advance to the next letter.

    Couldn't really try out the stylus for writing, it was hard to be super
    accurate with it, and there were problems with "focus." The stylus does
    have another advantages with pointing though.

    There was also a "software" keyboard that one could bring up, but seemed to
    want to deliver two characters each time one key was pressed, so it was
    abandoned.

    Navigating using the navigation keys isn't too bad... the navigation for
    them was a bit slower than what was going on for number input. The
    interesting thing is that navigation with the keys is that there is very
    little hesitation in the "navigation", no M's.

    However, there's a lot of navigation that one has to do. For example, to
    change the number, one must view a contact, then activate the context menu
    and choose to edit it, then walk down 14 "fields" to change the number.
    Alternatively one could "navigate backwards" and wrap around. For example,
    the quickest way to make a contact your "business card," you can do this
    with a reasonable gain. It might be better to make these choices in the
    context menu for the contact (or even in the list). The time it takes to
    delete a contact is much faster than the other standard things. (it can be
    done in about 5 seconds, whereas most other tasks need the at around 10
    before you are even at the right spot). 3 seconds of this are spent just
    selecting the item for edit after viewing it.

    Changing the category something is in is a bit of a mess as well, it's a
    lot of navigation just to get to it.

    Some of the time though is waiting for the system to respond though. Part
    of the lag in selecting an item is the time it takes for the phone to
    retrieve for the task.



Datebook stuff:
    Creating categories is slightly better navigation-wise.

    In general, there's usually a lot of stuff to add in an event. There's a
    lot of help in that there's combo boxes that have standard choices, but
    there's still a lot of "meta" navigation. This also makes you forget that
    you can do some things very easy. For example, you navigate down to a
    date-time field and you forget that you can easily type the time in, but
    you try to use the direction keys instead.

    Editing an event, was slightly better still required a lot of movement.

    It seems one idea for making things better would be to have some sort of
    way of accessing things quickly and cutting down the navigation.

    
Todo:
    In general, semi-quick to add a note.
    Still suffer from the idea that you should use the arrow keys to change times.
    Suffer from the same problem for the priority. :-(

    It *is* possible to use the stylus here to change the things like whether
    or not the task is complete or the priority of the task, and it's
    embarrassingly faster to do it with a stylus marking a task complete with
    the keys takes around 13 seconds, and about 1 with the stylus.

    Idea for alternate design where changing the priority or "done" status can
    be done with left and right arrow keys.
