/****************************************************************************
**
** Copyright (C) 1992-$THISYEAR$ $TROLLTECH$. All rights reserved.
**
** This file is part of the $MODULE$ of the Qt Toolkit.
**
** $TROLLTECH_DUAL_LICENSE$
**
** This file is provided AS IS with NO WARRANTY OF ANY KIND, INCLUDING THE
** WARRANTY OF DESIGN, MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE.
**
****************************************************************************/

#include "qvfb.h"

#include <QApplication>
#include <QRegExp>
#include <QSettings>
#include <qdebug.h>
#include <stdlib.h>
#include <stdio.h>
#include <signal.h>

void fn_quit_qvfb(int)
{
    // pretend that we have quit normally
    qApp->quit();
}


void usage( const char *app )
{
    printf( "Usage: %s [-width width] [-height height] [-depth depth] [-zoom zoom]"
	    "[-mmap] [-nocursor] [-qwsdisplay :id] [-skin skindirectory]\n"
	    "Supported depths: 1, 4, 8, 32\n", app );
}
int qvfb_protocol = 0;

int main( int argc, char *argv[] )
{
    Q_INIT_RESOURCE(qvfb);

    QApplication app( argc, argv );
    app.setOrganizationName(QLatin1String("Trolltech"));
    app.setApplicationName(QLatin1String("QVFB"));

    int width;
    bool readWidth = false;
    int height;
    bool readHeight = false;
    int depth;
    bool readDepth = false;
    int rotation;
    bool cursor;
    bool readCursor = false;
    double zoom;
    bool readZoom = false;
    QString displaySpec( ":0" );
    bool readDisplay = false;
    QString skin;
    bool readSkin = false;

    for ( int i = 1; i < argc; i++ ){
	QString arg = argv[i];
	if ( arg == "-width" ) {
	    width = atoi( argv[++i] );
            readWidth = true;
	} else if ( arg == "-height" ) {
	    height = atoi( argv[++i] );
            readHeight = true;
	} else if ( arg == "-skin" ) {
	    skin = argv[++i];
            readSkin = true;
	} else if ( arg == "-depth" ) {
	    depth = atoi( argv[++i] );
            readDepth = true;
	} else if ( arg == "-nocursor" ) {
	    cursor = false;
            readCursor = true;
	} else if ( arg == "-mmap" ) {
	    qvfb_protocol = 1;
	} else if ( arg == "-zoom" ) {
	    zoom = atof( argv[++i] );
            readZoom = true;
	} else if ( arg == "-qwsdisplay" ) {
	    displaySpec = argv[++i];
            readDisplay = true;
	} else {
	    printf( "Unknown parameter %s\n", arg.toLatin1().constData() );
	    usage( argv[0] );
	    exit(1);
	}
    }

    int displayId;
    // now that we've read in the command line options, read the rest from settings
    QSettings settings;
    settings.beginGroup(QLatin1String("display"));
    if (!readDisplay) {
        displayId = settings.value(QLatin1String("id"), 0).toInt();
        rotation = settings.value(QLatin1String("rotation"), 0).toInt();
    } else {
        QRegExp r( ":[0-9]+" );
        int m = r.indexIn( displaySpec, 0 );
        int len = r.matchedLength();
        if ( m >= 0 ) {
            displayId = displaySpec.mid( m+1, len-1 ).toInt();
        }
        QRegExp rotRegExp( "Rot[0-9]+" );
        m = rotRegExp.indexIn( displaySpec, 0 );
        len = r.matchedLength();
        if ( m >= 0 ) {
            rotation = displaySpec.mid( m+3, len-3 ).toInt();
        }
    }

    qDebug( "Using display %d", displayId );
    signal(SIGINT, fn_quit_qvfb);
    signal(SIGTERM, fn_quit_qvfb);

    if (!readWidth)
        width = settings.value(QLatin1String("width"), 0).toInt();
    if (!readHeight)
        height = settings.value(QLatin1String("height"), 0).toInt();
    if (!readDepth)
        depth = settings.value(QLatin1String("depth"), 32).toInt();
    settings.endGroup();

    if (!readSkin)
        skin = settings.value(QLatin1String("skin"), QString()).toString();
    if (!readZoom)
        zoom = settings.value(QLatin1String("zoom"), 1.0).toDouble();
    if (!readCursor)
        cursor = settings.value(QLatin1String("cursorEnabled"), true).toBool();


    QVFb mw( displayId, width, height, depth, rotation, skin );
    mw.setZoom(zoom);
    mw.enableCursor(cursor);
    mw.show();

    return app.exec();
}
