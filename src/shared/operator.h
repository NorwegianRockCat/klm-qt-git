/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef KLMOPERATOR_H
#define KLMOPERATOR_H

#include <QtCore/QDataStream>
#include <QtCore/QPoint>
#include <QtCore/QString>
#include <QtCore/QTime>

class KLMOperator
{
public:
    enum OperatorType { Mental, Homing, Point, KeyPress, KeyRelease, ButtonPress, ButtonRelease,
                         WaitStart, WaitStop, IMComposing, IMCommit};
    KLMOperator(OperatorType t, const QTime &when = QTime::currentTime(),
                int duration = 0)
     : mType(t), mOperatorTime(when), mDuration(duration) {}
    virtual ~KLMOperator() {}

    inline OperatorType type() const { return mType; }
    inline QTime operatorTime() const { return mOperatorTime; }
    inline int duration() const { return mDuration; }
    inline QString note() const { return mNote; }
    inline void setOperatorTime(const QTime &newTime) { mOperatorTime = newTime; }
    inline void setDuration(int newDuration) { mDuration = newDuration; }
    inline void setNote(const QString &note) { mNote = note; }

private:
    OperatorType mType;
    QTime mOperatorTime;
    int mDuration;
    QString mNote;
    friend QDataStream &operator<<(QDataStream &out, const KLMOperator &op);
    friend QDataStream &operator>>(QDataStream &in, KLMOperator &op);
};

class PointOperator : public KLMOperator
{
public:
    PointOperator(const QPoint &start, const QPoint &stop = QPoint(),
                  const QTime &startTime = QTime::currentTime(),
                  int duration = 0)
        : KLMOperator(Point, startTime, duration),
          mLocation(start), mStopLocation(stop)
    {}
    inline QPoint location() const { return mLocation; }
    inline QPoint stopLocation() const { return mStopLocation; }

private:
    QPoint mLocation;
    QPoint mStopLocation;
    friend QDataStream &operator<<(QDataStream &out, const PointOperator &op);
    friend QDataStream &operator>>(QDataStream &in, PointOperator &op);
};

class ButtonClickOperator : public KLMOperator
{
public:
    ButtonClickOperator(OperatorType t, Qt::MouseButtons buttons,
                        const QTime &startTime = QTime::currentTime(),
                        int duration = 0)
        : KLMOperator(t, startTime, duration), mButtons(buttons) {}
    inline Qt::MouseButtons buttons() const { return mButtons; }

private:
    Qt::MouseButtons mButtons;
    friend QDataStream &operator<<(QDataStream &out, const ButtonClickOperator &op);
    friend QDataStream &operator>>(QDataStream &in, ButtonClickOperator &op);
};

class KeyStrokeOperator : public KLMOperator
{
public:
    KeyStrokeOperator(OperatorType t, Qt::Key key, const QString &text,
                      const QTime &startTime = QTime::currentTime(),
                      int duration = 0)
        : KLMOperator(t, startTime, duration), mKey(key), mText(text) {}
    inline Qt::Key key() const { return mKey; }
    inline QString text() const { return mText; }

private:
    Qt::Key mKey;
    QString mText;
    friend QDataStream &operator<<(QDataStream &out, const KeyStrokeOperator &op);
    friend QDataStream &operator>>(QDataStream &in, KeyStrokeOperator &op);
};

class InputMethodOperator : public KLMOperator
{
public:
    InputMethodOperator(OperatorType t, const QString &text,
                        const QTime &startTime = QTime::currentTime(), int duration = 0)
        : KLMOperator(t, startTime, duration), mText(text) {}
    inline QString text() const { return mText; }
private:
    QString mText;
    friend QDataStream &operator<<(QDataStream &out, const InputMethodOperator &op);
    friend QDataStream &operator>>(QDataStream &in, InputMethodOperator &op);

};

static const quint8 KLMOPVERSION = 0x01;

inline QDataStream &operator<<(QDataStream &out, const KLMOperator &op)
{
    if (out.version() >= QDataStream::Qt_4_2) {
        quint8 version = KLMOPVERSION;
        quint16 opType = quint16(op.type());
        quint32 duration = op.duration();
        out << version << opType << op.operatorTime() << duration;
    }
    return out;
}

inline QDataStream &operator>>(QDataStream &in, KLMOperator &op)
{
    if (in.version() >= QDataStream::Qt_4_2) {
        quint8 version;
        quint16 type = 0;
        quint32 duration;
        in >> version;
        if (version != 0) {
            in >> type >> op.mOperatorTime >> duration;
        } else { // "version zero" we didn't record it, good thing we had MSB first
            quint8 tmp;
            in >> tmp;
            type = tmp;
            in >> op.mOperatorTime >> duration;
        }
        op.mType = KLMOperator::OperatorType(type);
        op.mDuration = duration;
    }
    return in;
}

inline QDataStream &operator<<(QDataStream &out, const PointOperator &op)
{
    if (out.version() >= QDataStream::Qt_4_2) {
        out << static_cast<const KLMOperator &>(op);
        out << op.location() << op.stopLocation();
    }
    return out;
}

inline QDataStream &operator>>(QDataStream &in, PointOperator &op)
{
    if (in.version() >= QDataStream::Qt_4_2) {
        in >> static_cast<KLMOperator &>(op);
        in >> op.mLocation >> op.mStopLocation;
    }
    return in;
}

inline QDataStream &operator<<(QDataStream &out, const ButtonClickOperator &op)
{
    if (out.version() >= QDataStream::Qt_4_2) {
        out << static_cast<const KLMOperator &>(op);
        out << qint32(op.buttons());
    }
    return out;
}

inline QDataStream &operator>>(QDataStream &in, ButtonClickOperator &op)
{
    if (in.version() >= QDataStream::Qt_4_2) {
        in >> static_cast<KLMOperator &>(op);
        qint32 buttons;
        in >> buttons;
        op.mButtons = Qt::MouseButtons(buttons);
    }
    return in;
}

inline QDataStream &operator<<(QDataStream &out, const KeyStrokeOperator &op)
{
    if (out.version() >= QDataStream::Qt_4_2) {
        out << static_cast<const KLMOperator &>(op);
        out << qint32(op.key()) << op.text();
    }
    return out;
}

inline QDataStream &operator>>(QDataStream &in, KeyStrokeOperator &op)
{
    if (in.version() >= QDataStream::Qt_4_2) {
        in >> static_cast<KLMOperator &>(op);
        qint32 key;
        in >> key;
        op.mKey = Qt::Key(key);
        in >> op.mText;
    }
    return in;
}


inline QDataStream &operator<<(QDataStream &out, const InputMethodOperator &op)
{
    if (out.version() >= QDataStream::Qt_4_2) {
        out << static_cast<const KLMOperator &>(op);
        out << op.text();
    }
    return out;
}

inline QDataStream &operator>>(QDataStream &in, InputMethodOperator &op)
{
    if (in.version() >= QDataStream::Qt_4_2) {
        in >> static_cast<KLMOperator &>(op);
        QString foo;
        in >> foo;
        op.mText = foo;
    }
    return in;
}

#endif //KLMOPERATOR_H
