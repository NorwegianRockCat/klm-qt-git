/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtNetwork/QTcpServer>
#include <QtNetwork/QTcpSocket>
#include <QtCore/QTimer>
#include <QtCore/QCoreApplication>
#include <QtCore/QFileInfo>
#include <QtCore/QTextStream>

#include "bonjourserviceregister.h"
#include "listenerserver.h"
#include "listener.h"
#include "operator.h"

static const quint16 MagicNumber = 0xEFEE;
static const int ServerTimeout = 30 * 1000;

//#define LISTENERSERVER_DEBUG
#ifdef LISTENERSERVER_DEBUG
#include <QtCore/QDebug>
#endif

ListenerServer::ListenerServer(Listener *parent)
    : QObject(parent), status(WaitingForConnection)
{
    mServerSocket = new QTcpServer(this);
    mServerSocket->setMaxPendingConnections(1);
    connect(mServerSocket, SIGNAL(newConnection()), this, SLOT(newConnection()));
    cleanUpOperators();
    mCurrentConnection = 0;
    if (!mServerSocket->listen()) {
        qWarning("Could not start server: %s", qPrintable(mServerSocket->errorString()));
        status = Error;
    } else {
        QString serviceName = QCoreApplication::instance()->applicationName();
        if (serviceName.isEmpty()) {
            serviceName = QFileInfo(QCoreApplication::instance()->applicationFilePath()).baseName();
        }
        if (!serviceName.isEmpty())
            serviceName += QLatin1String(" ");
        serviceName += QLatin1String("KLMServer");
        BonjourServiceRegistrar *serviceRegister = new BonjourServiceRegistrar(this);
        serviceRegister->registerService(BonjourRecord(serviceName,
                                                       QLatin1String("_klmqt._tcp"),
                                                       QString()),
                                         mServerSocket->serverPort());
        connect(serviceRegister, SIGNAL(error(int)), this, SLOT(registrarError(int)));
    }
    idleTimer = new QTimer(this);
    idleTimer->setInterval(ServerTimeout);
    idleTimer->setSingleShot(true);
    connect(idleTimer, SIGNAL(timeout()), this, SLOT(sayGoodbye()));
}

ListenerServer::~ListenerServer()
{
}

void ListenerServer::registrarError(DNSServiceErrorType err)
{
    qWarning("Error registering with bonjour %d", err);
}

void ListenerServer::newConnection()
{
#ifdef LISTENERSERVER_DEBUG
    qDebug() << "Got a connection";
#endif
    if (mCurrentConnection)  { // Wait until this session has finished.
#ifdef LISTENERSERVER_DEBUG
        qDebug() << "  returning because I already have one connection";
#endif
        return;
    }

    mCurrentConnection = mServerSocket->nextPendingConnection();
    connect(mCurrentConnection, SIGNAL(readyRead()), this, SLOT(readClientCommand()));
    connect(mCurrentConnection, SIGNAL(disconnected()), this, SLOT(cleanUpDisconnect()));
    toConnectedState();
}

void ListenerServer::readClientCommand()
{
    // Some stuff here!
    QByteArray input(1024, 0);
    while (mCurrentConnection->bytesAvailable()) {
        qint64 bytesRead = mCurrentConnection->readLine(input.data(), 1024);
        if (bytesRead == -1)  // Read Error, BAIL!
            break;
#ifdef LISTENERSERVER_DEBUG
    qDebug() << "read" << bytesRead << input;
#endif
        idleTimer->stop();
        processCommand(input);
        if (status == WaitingForConnection)
            break;
    }
}

void ListenerServer::sayGoodbye()
{
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "Goodbye!\r\n" << flush;
#ifdef LISTENERSERVER_DEBUG
    qDebug() << ">>>>>>>>>>>>>>>>>>>";
    qDebug() << block;
#endif
    mCurrentConnection->write(block);
    mCurrentConnection->disconnectFromHost();
    idleTimer->stop();
}

void ListenerServer::processCommand(const QByteArray &cmd)
{
    QString command = QString::fromUtf8(cmd).trimmed().toLower();
    if (command == QLatin1String("bye")) {  // bye is always valid
        sayGoodbye();
        return;
    }
    switch (status) {
    case WaitingForConnection:
    case Error: {
        QByteArray block;
        QTextStream out(&block, QIODevice::WriteOnly);
        out.setCodec("UTF-8");
        out << "Server in error state. Please disconnect and try again\r\n"
            << flush;
#ifdef LISTENERSERVER_DEBUG
        qDebug() << ">>>>>>>>>>>>>>>>>>>";
        qDebug() << block;
#endif
        mCurrentConnection->write(block);
        idleTimer->start();
        break;
    }
    case Connected:
        if (command == QLatin1String("start")) {
            startRecording();
        } else {
            unknownCommand();
        }
        break;
    case ReadyForSending:
    case Sending:
    case WaitingForOperators:
        if (command == QLatin1String("send")) {
            sendFrontOperator();
        } else if (command == QLatin1String("stop")) {
            stopRecording();
        } else {
            unknownCommand();
        }
        break;
    }
}

void ListenerServer::sendKLMOperator(const KLMOperator *klmOp)
{
    mOperatorsToSend.append(klmOp);
    if (status == WaitingForOperators)
        sendFrontOperator();
}

void ListenerServer::toConnectedState()
{
    QByteArray block;
    QTextStream out(&block);
    out.setCodec("UTF-8");
#ifdef LISTENERSERVER_DEBUG
    qDebug() << ">>>>>>>>>>>>>>>>>>>>>>";
    qDebug() << "sending OK\r\n";
#endif
    out << "OK\r\n" << flush;
    mCurrentConnection->write(block);
    status = Connected;
    idleTimer->start();
}

void ListenerServer::sendFrontOperator()
{
    if (mOperatorsToSend.isEmpty()) {
        switch (status) {
        case ReadyForSending:
        case Sending:
            status = WaitingForOperators;
            break;
        default:
            break;
        }
        return;
    }

    status = Sending;

    const KLMOperator *op = mOperatorsToSend.takeFirst();
    QByteArray block;
    QDataStream out(&block, QIODevice::WriteOnly);
    out.setVersion(QDataStream::Qt_4_2);
    out << quint16(0);
    out << MagicNumber;
    out << quint16(op->type());
    switch (op->type()) {
    case KLMOperator::Mental:
    case KLMOperator::Homing:
    case KLMOperator::WaitStart:
    case KLMOperator::WaitStop:
        out << op;
    case KLMOperator::Point: {
        out << *static_cast<const PointOperator *>(op);
        break;
    }
    case KLMOperator::KeyPress:
    case KLMOperator::KeyRelease: {
        out << *static_cast<const KeyStrokeOperator *>(op);
        break;
    }
    case KLMOperator::ButtonPress:
    case KLMOperator::ButtonRelease: {
        out << *static_cast<const ButtonClickOperator *>(op);
        break;
    }
    case KLMOperator::IMComposing:
    case KLMOperator::IMCommit: {
        out << *static_cast<const InputMethodOperator *>(op);
        break;
    }
    }
    out.device()->seek(0);
    out << quint16(block.size() - sizeof(quint16));


    QByteArray textBlock;
    {
        QTextStream textOut(&textBlock, QIODevice::WriteOnly);
        textOut.setCodec("UTF-8");
        textOut << "DATA " << block.size() << "\r\n" << flush;
    }
#ifdef LISTENERSERVER_DEBUG
            qDebug() << ">>>>>>>>>>>>>>>>>>>>>>";
            qDebug() << "sending " << textBlock;
            qDebug() << block;;
#endif
    mCurrentConnection->write(textBlock);
    mCurrentConnection->write(block);
    textBlock.truncate(0);
    {
        QTextStream textOut(&textBlock, QIODevice::WriteOnly);
        textOut.setCodec("UTF-8");
        textOut << "\r\n.\r\n" << flush;
    }
#ifdef LISTENERSERVER_DEBUG
    qDebug() << textBlock;
#endif
    mCurrentConnection->write(textBlock);
    ++mOperatorSentCount;
    idleTimer->start();

}

void ListenerServer::cleanUpDisconnect()
{
    status = WaitingForConnection;
    stopRecording();
    cleanUpOperators();
    if (Listener *listener = qobject_cast<Listener *>(parent())) {
        listener->clear();
    }
    mCurrentConnection->deleteLater();;
    mCurrentConnection = 0;
    idleTimer->stop();

    if (mServerSocket->hasPendingConnections()) {
#ifdef LISTENERSERVER_DEBUG
        qDebug() << " Still pending connections, grabbing next connection";
#endif
        newConnection();
    }
}

void ListenerServer::cleanUpOperators()
{
    mOperatorsToSend.clear();
    mOperatorSentCount = 0;
}


void ListenerServer::startRecording()
{
    emit needRecording(true);
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "RECORDING\r\n" << flush;
#ifdef LISTENERSERVER_DEBUG
    qDebug() << ">>>>>>>>>>>>>>>>>>>>>>>>";
    qDebug() << block;
#endif
    mCurrentConnection->write(block);
    status = ReadyForSending;
    idleTimer->start();
}

void ListenerServer::stopRecording()
{
    emit needRecording(false);
    if (mCurrentConnection) {
        QByteArray block;
        QTextStream out(&block, QIODevice::WriteOnly);
        out.setCodec("UTF-8");
        out << "STOPPED\r\n" << flush;
#ifdef LISTENERSERVER_DEBUG
    qDebug() << ">>>>>>>>>>>>>>>>>>>>>>>>";
    qDebug() << block;
#endif
        mCurrentConnection->write(block);
        status = Connected;
        idleTimer->start();
    }
}

void ListenerServer::unknownCommand()
{
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
#ifdef LISTENERSERVER_DEBUG
    qDebug() << ">>>>>>>>>>>>>>>>>>>>>>>>";
    qDebug() << "What?!?\r\n";
#endif
    out << "What?!?\r\n" << flush;
    mCurrentConnection->write(block);
    idleTimer->start();
}
