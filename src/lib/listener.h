/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LISTENER_H
#define LISTENER_H

#include <QtCore/QHash>
#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtCore/QAbstractEventDispatcher>
#include <QtCore/QTime>
#ifdef Q_WS_MAC
#include <Carbon/Carbon.h>
#endif

#ifdef QTOPIA_PHONE
#include "qtopiaglobal.h"
#else
#define QTOPIA_EXPORT
#endif

class KLMOperator;
class ListenerServer;
class QTOPIA_EXPORT Listener : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QAbstractEventDispatcher *dispatcher READ dispatcher WRITE setDispatcher DESIGNABLE false);
    Q_PROPERTY(bool recording READ isRecording WRITE setRecording)
public:
    Listener(QAbstractEventDispatcher *parent = 0);
    ~Listener();
    void setDispatcher(QAbstractEventDispatcher *target);
    QAbstractEventDispatcher *dispatcher() const;
    void addKLMOperator(KLMOperator *klmOp);
    bool isRecording() const { return mRecording; }
    QList<KLMOperator *> klmOperators() const { return klmOps; }
    void clear();
    static Listener *instance(QAbstractEventDispatcher *dispatcher
                                        = QAbstractEventDispatcher::instance());

public slots:
    void setRecording(bool record);

signals:
    void recordingStatusChanged(bool newStatus);

protected:
    static bool listenerEventFilter(void *event);

private slots:
    void addBlock();
    void addWakeUp();

private:
    static QHash<QAbstractEventDispatcher *, Listener *> listenerHash;
#ifdef Q_WS_MAC
    static OSStatus listenerEventHandler(EventHandlerCallRef ehcr, EventRef event, void *data);
    EventTime mStartCarbonTime;
    QTime mStartRecordingTime;
#endif
    void init_sys();
    void destroy_sys();
    void eventFilter_sys(void *event);
    void startRecording_sys();
    void stopRecording_sys();

    QAbstractEventDispatcher *mTarget;
    QAbstractEventDispatcher::EventFilter oldFilter;
    QList<KLMOperator *> klmOps;
    bool mRecording;
    ListenerServer *mServer;
};

#endif // LISTENER_H
