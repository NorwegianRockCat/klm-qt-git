/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "listener.h"
#include <QtCore/QDebug>
#include <QtCore/QTextCodec>

static inline Qt::MouseButton determineQtMouseButton(UInt32 button)
{
    Qt::MouseButton qtButton = Qt::NoButton;
    switch (button) {
    case kEventMouseButtonPrimary:
        qtButton = Qt::LeftButton;
        break;
    case kEventMouseButtonSecondary:
        qtButton = Qt::RightButton;
        break;
    case kEventMouseButtonTertiary:
        qtButton = Qt::MidButton;
        break;
    case 4:
        qtButton = Qt::XButton1;
        break;
    case 5:
        qtButton = Qt::XButton2;
        break;
    default:
        break;
    }
    return qtButton;
}

/* key maps */
struct qt_mac_enum_mapper
{
    int mac_code;
    int qt_code;
};
//keyboard keys (non-modifiers)
static qt_mac_enum_mapper qt_mac_keyboard_symbols[] = {
    { kHomeCharCode, (Qt::Key_Home) },
    { kEnterCharCode, (Qt::Key_Enter) },
    { kEndCharCode, (Qt::Key_End) },
    { kBackspaceCharCode, (Qt::Key_Backspace) },
    { kTabCharCode, (Qt::Key_Tab) },
    { kPageUpCharCode, (Qt::Key_PageUp) },
    { kPageDownCharCode, (Qt::Key_PageDown) },
    { kReturnCharCode, (Qt::Key_Return) },
    { kEscapeCharCode, (Qt::Key_Escape) },
    { kLeftArrowCharCode, (Qt::Key_Left) },
    { kRightArrowCharCode, (Qt::Key_Right) },
    { kUpArrowCharCode, (Qt::Key_Up) },
    { kDownArrowCharCode, (Qt::Key_Down) },
    { kHelpCharCode, (Qt::Key_Help) },
    { kDeleteCharCode, (Qt::Key_Delete) },
//ascii maps, for debug
    { ':', (Qt::Key_Colon) },
    { ';', (Qt::Key_Semicolon) },
    { '<', (Qt::Key_Less) },
    { '=', (Qt::Key_Equal) },
    { '>', (Qt::Key_Greater) },
    { '?', (Qt::Key_Question) },
    { '@', (Qt::Key_At) },
    { ' ', (Qt::Key_Space) },
    { '!', (Qt::Key_Exclam) },
    { '"', (Qt::Key_QuoteDbl) },
    { '#', (Qt::Key_NumberSign) },
    { '$', (Qt::Key_Dollar) },
    { '%', (Qt::Key_Percent) },
    { '&', (Qt::Key_Ampersand) },
    { '\'', (Qt::Key_Apostrophe) },
    { '(', (Qt::Key_ParenLeft) },
    { ')', (Qt::Key_ParenRight) },
    { '*', (Qt::Key_Asterisk) },
    { '+', (Qt::Key_Plus) },
    { ',', (Qt::Key_Comma) },
    { '-', (Qt::Key_Minus) },
    { '.', (Qt::Key_Period) },
    { '/', (Qt::Key_Slash) },
    { '[', (Qt::Key_BracketLeft) },
    { ']', (Qt::Key_BracketRight) },
    { '\\', (Qt::Key_Backslash) },
    { '_', (Qt::Key_Underscore) },
    { '`', (Qt::Key_QuoteLeft) },
    { '{', (Qt::Key_BraceLeft) },
    { '}', (Qt::Key_BraceRight) },
    { '|', (Qt::Key_Bar) },
    { '~', (Qt::Key_AsciiTilde) },
    { '^', (Qt::Key_AsciiCircum) },
    {   0, (0) }
};

static qt_mac_enum_mapper qt_mac_keyvkey_symbols[] = { //real scan codes
    { 122, (Qt::Key_F1) },
    { 120, (Qt::Key_F2) },
    { 99,  (Qt::Key_F3) },
    { 118, (Qt::Key_F4) },
    { 96,  (Qt::Key_F5) },
    { 97,  (Qt::Key_F6) },
    { 98,  (Qt::Key_F7) },
    { 100, (Qt::Key_F8) },
    { 101, (Qt::Key_F9) },
    { 109, (Qt::Key_F10) },
    { 103, (Qt::Key_F11) },
    { 111, (Qt::Key_F12) },
    {   0, (0) }
};

static int qt_mac_get_key(int modif, const QChar &key, int virtualKey)
{
    if (key == kClearCharCode && virtualKey == 0x47)
        return Qt::Key_Clear;

    if (key.isDigit()) {
        return key.digitValue() + Qt::Key_0;
    }

    if (key.isLetter()) {
        return (key.toUpper().unicode() - 'A') + Qt::Key_A;
    }
    if (key.isSymbol()) {
        return key.unicode();
    }

    for (int i = 0; qt_mac_keyboard_symbols[i].qt_code; i++) {
        if (qt_mac_keyboard_symbols[i].mac_code == key) {
            if (qt_mac_keyboard_symbols[i].qt_code
                    == Qt::Key_Tab && (modif & Qt::ShiftModifier)) {
                return Qt::Key_Backtab;
            }

            return qt_mac_keyboard_symbols[i].qt_code;
        }
    }

    for (int i = 0; qt_mac_keyvkey_symbols[i].qt_code; i++) {
        if (qt_mac_keyvkey_symbols[i].mac_code == virtualKey) {
            return qt_mac_keyvkey_symbols[i].qt_code;
        }
    }

    //oh well
    return Qt::Key_unknown;
}

static bool translateKeyEventInternal(EventRef keyEvent, int *qtKey,
                                      QChar *outChar,
                                      Qt::KeyboardModifiers *outModifiers)
{
    const UInt32 ekind = GetEventKind(keyEvent);

    UInt32 keyCode = 0;
    GetEventParameter(keyEvent, kEventParamKeyCode, typeUInt32, 0,
                      sizeof(keyCode), 0, &keyCode);

    //get mac mapping
    static UInt32 tmp_unused_state = 0L;
    KeyboardLayoutRef keyLayoutRef = 0;
    UCKeyboardLayout *uchrData = 0;
    KLGetCurrentKeyboardLayout(&keyLayoutRef);
    OSStatus err;
    if (keyLayoutRef != 0) {
        err = KLGetKeyboardLayoutProperty(keyLayoutRef, kKLuchrData,
                                  const_cast<const void **>(reinterpret_cast<void **>(&uchrData)));
        if (err != noErr) {
            qWarning("Unable to get keyboardlayout %ld %s:%d",
                     long(err), __FILE__, __LINE__);
        }
    }

    *qtKey = Qt::Key_unknown;
    if (uchrData) {
        // The easy stuff; use the unicode stuff!
        UniChar string[4];
        UniCharCount actualLength;
        UInt32 currentModifiers = GetCurrentEventKeyModifiers();
        UInt32 currentModifiersWOAltOrControl = currentModifiers & ~(controlKey | optionKey);
        int keyAction;
        switch (ekind) {
        default:
        case kEventRawKeyDown:
            keyAction = kUCKeyActionDown;
            break;
        case kEventRawKeyUp:
            keyAction = kUCKeyActionUp;
            break;
        case kEventRawKeyRepeat:
            keyAction = kUCKeyActionAutoKey;
            break;
        }
        OSStatus err = UCKeyTranslate(uchrData, keyCode, keyAction,
                                  ((currentModifiersWOAltOrControl >> 8) & 0xff), LMGetKbdType(),
                                  kUCKeyTranslateNoDeadKeysMask, &tmp_unused_state, 4, &actualLength,
                                  string);
        if (err == noErr) {
            *outChar = QChar(string[0]);
            *qtKey = qt_mac_get_key(*outModifiers, *outChar, keyCode);
            if (currentModifiersWOAltOrControl != currentModifiers) {
                // Now get the real char.
                err = UCKeyTranslate(uchrData, keyCode, keyAction,
                                     ((currentModifiers >> 8) & 0xff), LMGetKbdType(),
                                      kUCKeyTranslateNoDeadKeysMask, &tmp_unused_state, 4, &actualLength,
                                      string);
                if (err == noErr)
                    *outChar = QChar(string[0]);
            }
        } else {
            qWarning("Qt::internal::UCKeyTranslate is returnining %ld %s:%d",
                     long(err), __FILE__, __LINE__);
        }
    } else {
        // The road less travelled; use KeyTranslate
        void *keyboard_layout;
        KeyboardLayoutRef keyLayoutRef = 0;
        KLGetCurrentKeyboardLayout(&keyLayoutRef);
        err = KLGetKeyboardLayoutProperty(keyLayoutRef, kKLKCHRData,
                                  const_cast<const void **>(reinterpret_cast<void **>(&keyboard_layout)));

        int translatedChar = KeyTranslate(keyboard_layout, (GetCurrentEventKeyModifiers() &
                                                             (kEventKeyModifierNumLockMask|shiftKey|cmdKey|
                                                              rightShiftKey|alphaLock)) | keyCode,
                                           &tmp_unused_state);
        if (!translatedChar) {
            return false;
        }

        //map it into qt keys
        *qtKey = qt_mac_get_key(*outModifiers, QChar(translatedChar), keyCode);
        if (*outModifiers & (Qt::AltModifier | Qt::ControlModifier)) {
            if (translatedChar & (1 << 7)) //high ascii
                translatedChar = 0;
        } else {          //now get the real ascii value
            UInt32 tmp_mod = 0L;
            static UInt32 tmp_state = 0L;
            if (*outModifiers & Qt::ShiftModifier)
                tmp_mod |= shiftKey;
            if (*outModifiers & Qt::MetaModifier)
                tmp_mod |= controlKey;
            if (*outModifiers & Qt::ControlModifier)
                tmp_mod |= cmdKey;
            if (GetCurrentEventKeyModifiers() & alphaLock) //no Qt mapper
                tmp_mod |= alphaLock;
            if (*outModifiers & Qt::AltModifier)
                tmp_mod |= optionKey;
            if (*outModifiers & Qt::KeypadModifier)
                tmp_mod |= kEventKeyModifierNumLockMask;
            translatedChar = KeyTranslate(keyboard_layout, tmp_mod | keyCode, &tmp_state);
        }
        {
            ByteCount unilen = 0;
            if (GetEventParameter(keyEvent, kEventParamKeyUnicodes, typeUnicodeText, 0, 0, &unilen, 0)
                    == noErr && unilen == 2) {
                GetEventParameter(keyEvent, kEventParamKeyUnicodes, typeUnicodeText, 0, unilen, 0, outChar);
            } else if (translatedChar) {
                static QTextCodec *c = 0;
                if (!c)
                    c = QTextCodec::codecForName("Apple Roman");
		char tmpChar = (char)translatedChar; // **sigh**
                *outChar = c->toUnicode(&tmpChar, 1).at(0);
            } else {
                *qtKey = qt_mac_get_key(*outModifiers, QChar(translatedChar), keyCode);
            }
        }
    }
    if (*qtKey == Qt::Key_unknown)
        *qtKey = qt_mac_get_key(*outModifiers, *outChar, keyCode);
    return true;
}

static EventTypeSpec listener_events[] = {
    { kEventClassMouse, kEventMouseDown },
    { kEventClassMouse, kEventMouseDown },
    { kEventClassMouse, kEventMouseDragged },
    { kEventClassMouse, kEventMouseMoved },
    { kEventClassKeyboard, kEventRawKeyModifiersChanged },
    { kEventClassKeyboard, kEventRawKeyRepeat },
    { kEventClassKeyboard, kEventRawKeyUp },
    { kEventClassKeyboard, kEventRawKeyDown },
};

static EventHandlerUPP listener_proc_handlerUPP = 0;
static EventHandlerRef listenerHandler = 0;

OSStatus Listener::listenerEventHandler(EventHandlerCallRef, EventRef event, void *data)
{
    Listener *dispatcherListener = static_cast<Listener *>(data);
    if (!dispatcherListener) {
        qWarning("Could not find a listener for this instance of the dispatcher!");
    }


    if (dispatcherListener->isRecording()) {
        dispatcherListener->eventFilter_sys(&event);
    }

    return eventNotHandledErr;
}

void Listener::startRecording_sys()
{
    mStartCarbonTime = GetCurrentEventTime();
    mStartRecordingTime = QTime::currentTime();
}

void Listener::stopRecording_sys()
{
    mStartCarbonTime = 0;
    mStartRecordingTime = QTime();
}

void Listener::init_sys()
{
    if (!listener_proc_handlerUPP) {
        listener_proc_handlerUPP = NewEventHandlerUPP(Listener::listenerEventHandler);
        InstallEventHandler(GetEventDispatcherTarget(), listener_proc_handlerUPP,
                            GetEventTypeCount(listener_events), listener_events, this,
                            &listenerHandler);
    }
}

void Listener::destroy_sys()
{
    if (listener_proc_handlerUPP) {
        RemoveEventHandler(listenerHandler);
        DisposeEventHandlerUPP(listener_proc_handlerUPP);
        listener_proc_handlerUPP = 0;
    }
}

void Listener::eventFilter_sys(void *e)
{
    EventRef event = *(static_cast<EventRef *>(e));
    UInt32 eclass = GetEventClass(event);
    UInt32 ekind = GetEventKind(event);
    EventTime eTime = GetEventTime(event);
    QTime klmTime = mStartRecordingTime.addMSecs(int((eTime - mStartCarbonTime) * 1000));

    if (eclass == kEventClassMouse || eclass == kEventClassKeyboard) {
        static const int kEventParamListenerSeenEvent = 'KLMS';
        // Check if we've seen the event, if we have we shouldn't process
        // it again as it may lead to spurious "double events"
        bool seenEvent;
        if (GetEventParameter(event, kEventParamListenerSeenEvent,
                    typeBoolean, 0, sizeof(bool), 0, &seenEvent) == noErr) {
            if (seenEvent) {
                return;
            }
        }
        seenEvent = true;
        SetEventParameter(event, kEventParamListenerSeenEvent, typeBoolean,
                sizeof(bool), &seenEvent);
    }

    switch (eclass) {
    case kEventClassMouse:
        switch (ekind) {
        case kEventMouseDown:
        case kEventMouseUp: {
            UInt32 button;
            GetEventParameter(event, kEventParamMouseButton, typeUInt32, 0, sizeof(UInt32),
                              0, &button);
            Qt::MouseButtons qtButtons = determineQtMouseButton(button);
            ButtonClickOperator *press
                        = new ButtonClickOperator(
                                        ekind == kEventMouseDown ? KLMOperator::ButtonPress
                                                                 : KLMOperator::ButtonRelease,
                                                                  qtButtons, klmTime);
            addKLMOperator(press);
            break;
        }
        case kEventMouseMoved:
        case kEventMouseDragged: {
            Point pt;
            GetEventParameter(event, kEventParamMouseLocation, typeQDPoint, 0, sizeof(Point),
                              0, &pt);
            addKLMOperator(new PointOperator(QPoint(pt.h, pt.v), QPoint(), klmTime));
            break;
        }
        default:
            break;
        }
        break;
    case kEventClassKeyboard:
        switch (ekind) {
        case kEventRawKeyDown:
        case kEventRawKeyRepeat:
        case kEventRawKeyUp: {
            int qtKey;
            QChar code;
            Qt::KeyboardModifiers modifiers;
            translateKeyEventInternal(event, &qtKey, &code, &modifiers);
            if (ekind != kEventRawKeyRepeat) {
                addKLMOperator(new KeyStrokeOperator(
                                                ekind == kEventRawKeyDown ? KLMOperator::KeyPress
                                                                          : KLMOperator::KeyRelease,
                                                                         Qt::Key(qtKey), QString(code),
                                                                         klmTime));
            } else {
                addKLMOperator(new KeyStrokeOperator(KLMOperator::KeyPress, Qt::Key(qtKey),
                                                     QString(code), klmTime));
                addKLMOperator(new KeyStrokeOperator(KLMOperator::KeyRelease, Qt::Key(qtKey),
                                                     QString(code), klmTime));
            }
            break;
        }
        case kEventRawKeyModifiersChanged: {
            static UInt32 cachedModifiers = 0;
            UInt32 modifiers;
            GetEventParameter(event, kEventParamKeyModifiers, typeUInt32, 0, sizeof(UInt32),
                              0, &modifiers);
            UInt32 lastModifiers = cachedModifiers;
            UInt32 changedModifiers = lastModifiers ^ modifiers;
            cachedModifiers = modifiers;
            static const qt_mac_enum_mapper modifier_key_symbols[] = {
                { shiftKeyBit, (Qt::Key_Shift) },
                { rightShiftKeyBit, (Qt::Key_Shift) },
                { controlKeyBit, (Qt::Key_Meta) },
                { rightControlKeyBit, (Qt::Key_Meta) },
                { cmdKeyBit, (Qt::Key_Control) },
                { optionKeyBit, (Qt::Key_Alt) },
                { rightOptionKeyBit, (Qt::Key_Alt) },
                { alphaLockBit, (Qt::Key_CapsLock) },
                { kEventKeyModifierNumLockBit, (Qt::Key_NumLock) },
                { 0, (0) } };
            KLMOperator::OperatorType type;
            // walk through the bits
            for (int bit = 0; bit < 32; ++bit) {
                int testBit = (1 << bit);
                if (!(testBit & changedModifiers))
                    continue;
                type = (lastModifiers & testBit) ? KLMOperator::KeyRelease
                                                 : KLMOperator::KeyPress;
                for (int x = 0; modifier_key_symbols[x].mac_code != 0; ++x) {
                    if (modifier_key_symbols[x].mac_code == bit) {
                        addKLMOperator(new KeyStrokeOperator(type,
                                                        Qt::Key(modifier_key_symbols[x].qt_code),
                                                        QString(), klmTime));
                        break;
                    }
                }
            }
            break;
        }

        default:
            break;
        }
        break;
    default:
        break;
    }
}
