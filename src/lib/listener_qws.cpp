/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "listener.h"
#include "operator.h"
#include <QtGui/qwsevent_qws.h>
#include <QtGui/QInputMethodEvent>
#include <QtCore/QDebug>

void Listener::init_sys()
{
    // Nothing
}

void Listener::destroy_sys()
{
    // Nothing
}

void Listener::startRecording_sys()
{
    // Nothing
}

void Listener::stopRecording_sys()
{
    // Nothing
}

void Listener::eventFilter_sys(void *e)
{
    QWSEvent *event = static_cast<QWSEvent *>(e);
    switch (event->type) {
    case QWSEvent::Mouse: {
        static int cachedButtonState = 0;
        QWSMouseEvent *mouseEvent = static_cast<QWSMouseEvent *>(event);

        int lastButtonState = cachedButtonState;
        cachedButtonState = mouseEvent->simpleData.state;
        if (lastButtonState == cachedButtonState) {
            // Nothing has changed button-wise, so it must be a Point operator
            addKLMOperator(new PointOperator(QPoint(mouseEvent->simpleData.x_root,
                                             mouseEvent->simpleData.y_root)));
        } else {
            KLMOperator::OperatorType type;
            int changedButtons = lastButtonState ^ cachedButtonState;
            for (int bit = 1; bit < Qt::MouseButtonMask; bit = bit << 1) {
                if (!(bit & changedButtons))
                    continue;
                type = (lastButtonState & bit) ? KLMOperator::ButtonRelease
                                               : KLMOperator::ButtonPress;
                addKLMOperator(new ButtonClickOperator(type, Qt::MouseButtons(bit)));
            }
        }

        break;
    }
    case QWSEvent::Key:{
        QWSKeyEvent *keyEvent = static_cast<QWSKeyEvent *>(event);
        addKLMOperator(new KeyStrokeOperator(keyEvent->simpleData.is_press ? KLMOperator::KeyPress
                                                                           : KLMOperator::KeyRelease,
                                             Qt::Key(keyEvent->simpleData.keycode),
                                             QString(keyEvent->simpleData.unicode)));
        break;
    }
    case QWSEvent::IMEvent: {
        QWSIMEvent *imEvent = static_cast<QWSIMEvent *>(event);
        QDataStream stream(imEvent->streamingData);
        QString preedit;
        QString commit;
        stream >> preedit;
        stream >> commit;
        QList<QInputMethodEvent::Attribute> attrs;

        if (preedit.isEmpty() && !commit.isEmpty()) {
            addKLMOperator(new InputMethodOperator(KLMOperator::IMCommit, commit));
        } else {
            addKLMOperator(new InputMethodOperator(KLMOperator::IMComposing, preedit));
        }
        break;
    }
    default:
        break;
    }
}
