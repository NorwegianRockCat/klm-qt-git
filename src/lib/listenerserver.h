/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LISTENERSERVER_H
#define LISTENERSERVER_H

#include <QtCore/QList>
#include <QtCore/QObject>

class QSocketNotifier;
class QTcpServer;
class QTcpSocket;
class QTimer;
class KLMOperator;
class Listener;

class ListenerServer : public QObject
{
    Q_OBJECT
public:
    ListenerServer(Listener *parent);
    ~ListenerServer();
    enum ServerState { WaitingForConnection, Connected, ReadyForSending,
                       WaitingForOperators, Sending, Error };

    void sendKLMOperator(const KLMOperator *klmOp);

signals:
    void needRecording(bool);

private slots:
    void newConnection();
    void readClientCommand();
    void cleanUpDisconnect();
    void cleanUpOperators();
    void registrarError(int err);
    void sayGoodbye();

private:
    void processCommand(const QByteArray &command);
    void startRecording();
    void stopRecording();
    void sendFrontOperator();
    void unknownCommand();
    void toConnectedState();

    ServerState status;
    QList<const KLMOperator *> mOperatorsToSend;
    QTcpServer *mServerSocket;
    QTcpSocket *mCurrentConnection;
    int mOperatorSentCount;
    QTimer *idleTimer;
};

#endif // LISTENERSERVER_H
