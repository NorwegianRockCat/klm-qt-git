/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtCore/QDateTime>
#include <QtCore/QEvent>
#include <QtCore/QDebug>


#include "listener.h"
#include "listenerserver.h"
#include "operator.h"

QHash<QAbstractEventDispatcher *, Listener *> Listener::listenerHash;

Listener::Listener(QAbstractEventDispatcher *parent)
    : QObject(parent), mTarget(0), mRecording(false)
{
    setDispatcher(parent);
    mServer = new ListenerServer(this);
    connect(mServer, SIGNAL(needRecording(bool)), this, SLOT(setRecording(bool)));
    init_sys();
}

Listener::~Listener()
{
    destroy_sys();
    delete mServer;
}

void Listener::setDispatcher(QAbstractEventDispatcher *target)
{
    if (mTarget) {
        mTarget->setEventFilter(oldFilter);
        listenerHash.remove(mTarget);
        clear();
    }

    mTarget = target;
    if (mTarget) {
        oldFilter = mTarget->setEventFilter(Listener::listenerEventFilter);
        listenerHash.insert(mTarget, this);
    }
}

void Listener::clear()
{
    qDeleteAll(klmOps);
    klmOps.clear();
}

void Listener::setRecording(bool record)
{
    if (record == mRecording)
        return;

    mRecording = record;
    if (mRecording)
        startRecording_sys();
    else
        stopRecording_sys();
    emit recordingStatusChanged(mRecording);
}

void Listener::addBlock()
{
    addKLMOperator(new KLMOperator(KLMOperator::WaitStart));
}

void Listener::addWakeUp()
{
    addKLMOperator(new KLMOperator(KLMOperator::WaitStop));
}

QAbstractEventDispatcher *Listener::dispatcher() const
{
    return mTarget;
}

void Listener::addKLMOperator(KLMOperator *klmOp)
{
   klmOps.append(klmOp);
   mServer->sendKLMOperator(klmOp);
}

Listener *Listener::instance(QAbstractEventDispatcher *dispatcher)
{
    return listenerHash.value(dispatcher);
}

bool Listener::listenerEventFilter(void *e)
{
    Listener *dispatcherListener = instance();
    if (!dispatcherListener) {
        qWarning("Could not find a listener for this instance of the dispatcher!");
        return false;
    }

    if (dispatcherListener->isRecording())
        dispatcherListener->eventFilter_sys(e);
    return false; // We never want to eat the event, just examine it.
}

