#qtopia_project(lib qtopia)
QT += corelib gui network
HEADERS += listener.h listenerserver.h bonjourserviceregister.h
SOURCES += listener.cpp listenerserver.cpp bonjourserviceregister.cpp

PRECOMPILED_HEADER = klm-generator_pch.h
CONFIG += precompile_header no_objective_c
INCLUDEPATH += ../shared
TEMPLATE = lib
DESTDIR = ../../lib
TARGET = klmgenerator


!mac:!windows: {
    LIBS += -ldns_sd
}
windows: {
    LIBS += -ldnssd
}
embedded {
    SOURCES += listener_qws.cpp
}

mac:!embedded {
    SOURCES += listener_mac.cpp
    LIBS += -framework Carbon
}
