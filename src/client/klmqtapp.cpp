/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "klmqtapp.h"
#include "klmqtcontroller.h"
#include <QtGui/QCloseEvent>
#include <QtGui/QFileOpenEvent>
#include <QtGui/QPrinter>

QPrinter *KLMQtApplication::mPrinter = 0;
KLMQtController *KLMQtApplication::mController = 0;

KLMQtApplication::KLMQtApplication(int &argc, char **argv)
    : QApplication(argc, argv)
{
    setOrganizationDomain("ifi.uio.no");
    setOrganizationName("University Of Oslo");
    setApplicationName("KLM-Qt Viewer");
}

bool KLMQtApplication::event(QEvent *event)
{
    bool eaten;
    switch (event->type()) {
    default:
        eaten = QApplication::event(event);
        break;
    case QEvent::Close: {
        QCloseEvent *closeEvent = static_cast<QCloseEvent *>(event);
        closeEvent->setAccepted(mController->handleClose());
        if (closeEvent->isAccepted()) {
            eaten = QApplication::event(event);
        } else {
            eaten = true;
        }
        break; }
    case QEvent::FileOpen: {
        QFileOpenEvent *fileOpenEvent = static_cast<QFileOpenEvent *>(event);
        mController->openModel(fileOpenEvent->file());
        eaten = true;
        break; }
    }
    return eaten;
}

QPrinter *KLMQtApplication::printer()
{
    if (!mPrinter)
        mPrinter = new QPrinter(QPrinter::HighResolution);
    return mPrinter;
}

QString KLMQtApplication::keyToString(int key)
{
    switch (key) {
    case Qt::Key_Control:
        return QLatin1String("Control");
    case Qt::Key_Alt:
        return QLatin1String("Alt");
    case Qt::Key_AltGr:
        return QLatin1String("Alt Gr");
    case Qt::Key_Shift:
        return QLatin1String("Shift");
    case Qt::Key_Meta:
        return QLatin1String("Meta");
    case Qt::Key_Tab:
        return QLatin1String("Tab");
    case Qt::Key_CapsLock:
        return QLatin1String("Caps Lock");
    case Qt::Key_Up:
        return QLatin1String("Up");
    case Qt::Key_Down:
        return QLatin1String("Down");
    case Qt::Key_Left:
        return QLatin1String("Left");
    case Qt::Key_Right:
        return QLatin1String("Right");
    case Qt::Key_Enter:
        return QLatin1String("Enter");
    case Qt::Key_Return:
        return QLatin1String("Return");
    case Qt::Key_F1:
        return QLatin1String("F1");
    case Qt::Key_F2:
        return QLatin1String("F2");
    case Qt::Key_F3:
        return QLatin1String("F3");
    case Qt::Key_F4:
        return QLatin1String("F4");
    case Qt::Key_F5:
        return QLatin1String("F5");
    case Qt::Key_F6:
        return QLatin1String("F6");
    case Qt::Key_F7:
        return QLatin1String("F7");
    case Qt::Key_F8:
        return QLatin1String("F8");
    case Qt::Key_F9:
        return QLatin1String("F9");
    case Qt::Key_F10:
        return QLatin1String("F10");
    case Qt::Key_F11:
        return QLatin1String("F11");
    case Qt::Key_F12:
        return QLatin1String("F12");
    case Qt::Key_Delete:
        return QLatin1String("Delete");
    case Qt::Key_Insert:
        return QLatin1String("Insert");
    case Qt::Key_Home:
        return QLatin1String("Home");
    case Qt::Key_End:
        return QLatin1String("End");
    case Qt::Key_PageUp:
        return QLatin1String("PageUp");
    case Qt::Key_PageDown:
        return QLatin1String("PageDown");
    case Qt::Key_Escape:
        return QLatin1String("Escape");
    case Qt::Key_Select:
        return QLatin1String("Select");
    case Qt::Key_Yes:
        return QLatin1String("Yes");
    case Qt::Key_No:
        return QLatin1String("No");
    case Qt::Key_Context1:
        return QLatin1String("Context1");
    case Qt::Key_Context2:
        return QLatin1String("Context2");
    case Qt::Key_Context3:
        return QLatin1String("Context3");
    case Qt::Key_Context4:
        return QLatin1String("Context4");
    case Qt::Key_Call:
        return QLatin1String("Call");
    case Qt::Key_Hangup:
        return QLatin1String("Hangup");
    case Qt::Key_Flip:
        return QLatin1String("Flip");
    case Qt::Key_unknown:
        return QLatin1String("unknown");
    case Qt::Key_Back:
        return QLatin1String("Back");
    case Qt::Key_Forward:
        return QLatin1String("Forward");
    case Qt::Key_Stop:
        return QLatin1String("Stop");
    case Qt::Key_Refresh:
        return QLatin1String("Refresh");
    }
    return QString();
}
