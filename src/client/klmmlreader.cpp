/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "klmmlreader.h"
#include "klmtablemodel.h"
#include "operator.h"
#include <QtCore/QHash>
#include <QtCore/QVariant>

KLMMLReader::KLMMLReader()
    : mVersion(-1)
{
}

static const int CurrentKLMversion = 3;

int KLMMLReader::klmMLVersion()
{
    return CurrentKLMversion;
}

bool KLMMLReader::read(QIODevice *device)
{
    setDevice(device);
    while (!atEnd()) {
        readNext();
        if (isStartElement()) {
            if (name() != QLatin1String("klmML")) {
                raiseError(KLMTableModel::tr("This file is not an klmML file."));
            } else {
                mVersion = attributes().value(QLatin1String("klmVersion")).toString().toInt();
                if (mVersion > CurrentKLMversion) {
                    raiseError(KLMTableModel::tr("This file was created"
                                         " with a newer version of KLMQt and can't be read"
                                         " by this KLMQt.").arg(mVersion));
                } else {
                    readXML();
                }
            }
        }
    }
    return !error();
}

void KLMMLReader::readUnknownElement()
{
    Q_ASSERT(isStartElement());

    while (!atEnd()) {
        readNext();
        if (isEndElement())
            break;
        if (isStartElement())
            readUnknownElement();
    }
}

QTime KLMMLReader::readElementTime()
{
    Q_ASSERT(isStartElement());
    int hour, min, sec, msec;
    QString milliSecondTag = (mVersion > 2) ? QLatin1String("Millisecond")
                                            : QLatin1String("Milisecond");

    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;

        if (isStartElement()) {
            if (name() == QLatin1String("Hour")) {
                hour = readElementInt();
            } else if (name() == QLatin1String("Minute")) {
                min = readElementInt();
            } else if (name() == QLatin1String("Second")) {
                sec = readElementInt();
            } else if (name() == milliSecondTag) {
                msec = readElementInt();
            } else {
                readUnknownElement();
            }
        }
    }
     return QTime(hour, min, sec).addMSecs(msec);
}

int KLMMLReader::readElementInt()
{
    Q_ASSERT(isStartElement());
    return readElementText().toInt();
}

QPoint KLMMLReader::readElementPoint()
{
    Q_ASSERT(isStartElement());
    QPoint point;
    while (!atEnd()) {
        readNext();
        if (isEndElement())
            break;
        if (isStartElement()) {
            if (name() == QLatin1String("x")) {
                point.setX(readElementInt());
            } else if (name() == QLatin1String("y")) {
                point.setY(readElementInt());
            } else {
                readUnknownElement();
            }
        }
    }
    return point;
}

void KLMMLReader::readXML()
{
    Q_ASSERT(isStartElement() && name() == QLatin1String("klmML"));
    while (!atEnd()) {
        readNext();
        if (isEndElement())
            break;
        if (isStartElement()) {
            if (name() == QLatin1String("klmList"))
                readKlmList();
            else
                readUnknownElement();
        }
    }
}

void KLMMLReader::readKlmList()
{
    Q_ASSERT(isStartElement() && name() == QLatin1String("klmList"));
    mTaskName = attributes().value(QLatin1String("task")).toString();

    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;

        if (isStartElement()) {
            if (name() == QLatin1String("Operator"))
                readOperator();
            else
                readUnknownElement();
        }
    }
}

void KLMMLReader::readOperator()
{
    Q_ASSERT(isStartElement() && name() == QLatin1String("Operator"));
    QHash<QString, QVariant> operatorHash;
    while (!atEnd()) {
        readNext();

        if (isEndElement())
            break;

        if (isStartElement()) {
            if (name() == QLatin1String("type")
                    || name() == QLatin1String("key")
                    || name() == QLatin1String("buttons")
                    || name() == QLatin1String("duration")) {
                operatorHash.insert(name().toString(), readElementInt());
            } else if (name() == QLatin1String("operatorTime")) {
                operatorHash.insert(name().toString(), readElementTime());
            } else if (name() == QLatin1String("string")
                       || name() == QLatin1String("note")) {
                operatorHash.insert(name().toString(), readElementText());
            } else if (name() == QLatin1String("location")
                    || name() == QLatin1String("stopLocation")) {
                operatorHash.insert(name().toString(), readElementPoint());
            } else {
                readUnknownElement();
            }
        }
    }

    KLMOperator::OperatorType type = KLMOperator::OperatorType(operatorHash.value(QLatin1String("type")).toInt());
    QTime opTime = operatorHash.value(QLatin1String("operatorTime")).toTime();
    int duration = operatorHash.value(QLatin1String("duration")).toInt();

    KLMOperator *opToAppend;
    switch (type) {
    case KLMOperator::Mental:
    case KLMOperator::Homing:
    case KLMOperator::WaitStart:
    case KLMOperator::WaitStop:
        opToAppend = new KLMOperator(type, opTime, duration);
        break;
    case KLMOperator::IMComposing:
    case KLMOperator::IMCommit: {
        QString string = operatorHash.value(QLatin1String("string")).toString();
        opToAppend = new InputMethodOperator(type, string, opTime, duration);
        break;
    }
    case KLMOperator::KeyPress:
    case KLMOperator::KeyRelease: {
        Qt::Key key = Qt::Key(operatorHash.value(QLatin1String("key")).toInt());
        QString string = operatorHash.value(QLatin1String("string")).toString();
        opToAppend = new KeyStrokeOperator(type, key, string, opTime, duration);
        break;
    }
    case KLMOperator::ButtonPress:
    case KLMOperator::ButtonRelease: {
        Qt::MouseButtons buttons = Qt::MouseButtons(operatorHash.value(QLatin1String("buttons")).toInt());
        opToAppend = new ButtonClickOperator(type, buttons, opTime, duration);
        break;
    }
    case KLMOperator::Point: {
        QPoint startPos = operatorHash.value(QLatin1String("location")).toPoint();
        QPoint endPos = operatorHash.value(QLatin1String("stopLocation")).toPoint();
        opToAppend = new PointOperator(startPos, endPos, opTime, duration);
        break;
    }
    }
    opToAppend->setNote(operatorHash.value(QLatin1String("note")).toString());
    mOps.append(opToAppend);
}


QList<KLMOperator *> KLMMLReader::ops() const
{
    return mOps;
}

QString KLMMLReader::taskName() const
{
    return mTaskName;
}
