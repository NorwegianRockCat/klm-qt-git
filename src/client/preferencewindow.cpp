/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "preferencewindow.h"
#include <QtCore/QSettings>
#include <QtGui/QCheckBox>

PreferenceWindow::PreferenceWindow(QWidget *parent)
    : QWidget(parent, Qt::Window | Qt::CustomizeWindowHint | Qt::WindowSystemMenuHint)
{
    setAttribute(Qt::WA_DeleteOnClose);
    QSettings settings;
    setupUi(this);
    spinH->setValue(settings.value(QLatin1String("H-maximum"), 400).toInt());
    spinM->setValue(settings.value(QLatin1String("M-threshold"), 1200).toInt());
    autoHCheckBox->setChecked(settings.value(QLatin1String("auto-add-H"), true).toBool());
#ifdef Q_WS_MAC
    closeButton->hide();
#endif
}

PreferenceWindow::~PreferenceWindow()
{
}

void PreferenceWindow::on_spinM_valueChanged(int newValue)
{
    QSettings settings;
    if (settings.value(QLatin1String("M-threshold"), 1200).toInt() != newValue)
        settings.setValue(QLatin1String("M-threshold"), newValue);
}

void PreferenceWindow::on_spinH_valueChanged(int newValue)
{
    QSettings settings;
    if (settings.value(QLatin1String("H-maximum"), 400).toInt() != newValue)
        settings.setValue(QLatin1String("H-maximum"), newValue);
}

void PreferenceWindow::on_autoHCheckBox_toggled(bool newState)
{
    QSettings settings;
    if (settings.value(QLatin1String("auto-add-H"), true).toBool() != newState)
        settings.setValue(QLatin1String("auto-add-H"), newState);
}