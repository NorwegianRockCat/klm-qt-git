/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#include <QtCore/QTextStream>
#include <QtCore/QDataStream>
#include <QtNetwork/QHostInfo>
#include <QtNetwork/QHostAddress>
#include <QtNetwork/QTcpSocket>

#include "listenerclient.h"
#include "operator.h"

//#define LISTENERCLIENT_DEBUG

#ifdef LISTENERCLIENT_DEBUG
#include <QtCore/QDebug>
#endif

ListenerClient::ListenerClient(QObject *parent)
    : QObject(parent), status(Disconnected), mRecordingStatus(NotRecording),
      dataSize(-1)
{
    mSocket = new QTcpSocket(this);
    connect(mSocket, SIGNAL(readyRead()), this, SLOT(readCommand()));
    connect(mSocket, SIGNAL(connected()), this, SLOT(connectedToHost()));
    connect(mSocket, SIGNAL(disconnected()), this, SLOT(disconnectedFromHost()));
    connect(mSocket, SIGNAL(error(QAbstractSocket::SocketError)), this, SLOT(socketError(QAbstractSocket::SocketError)));
}

void ListenerClient::tryDisconnect()
{
    if (mSocket->state() == QAbstractSocket::ConnectedState) {
        sayBye();
        mSocket->waitForDisconnected();
    }
}

ListenerClient::~ListenerClient()
{
    tryDisconnect();
}

void ListenerClient::updateRecordingStatus(RecordingStatus newStatus)
{
    if (newStatus == mRecordingStatus)
        return;

    mRecordingStatus = newStatus;
    emit recordingStatusChanged(mRecordingStatus);
}

void ListenerClient::connectToServer(const QHostInfo &hostInfo, int port)
{
    const QList<QHostAddress> &addresses = hostInfo.addresses();
    if (!addresses.isEmpty()) {
#ifdef LISTENERCLIENT_DEBUG
        qDebug() << " attempt to connect to " << addresses.first() << ":" << port;
#endif
        tryDisconnect();
        mSocket->connectToHost(addresses.first(), port);
        updateRecordingStatus(Waiting);
    } else {
#ifdef LISTENERCLIENT_DEBUG
        qDebug() << "could not find the host address!";
#endif
        qWarning("could not any host");
    }
}

void ListenerClient::readCommand()
{
    QByteArray block(1024, 0);
    while (mSocket->bytesAvailable()) {
        qint64 bytesRead;
        if (status == ReceivingData && dataSize > 0) {
            int totalBytes = 0;
            while (totalBytes < dataSize) {
                bytesRead = mSocket->read(block.data() + totalBytes, dataSize - totalBytes);
                totalBytes += bytesRead;
            }
            bytesRead = totalBytes;
        } else {
            bytesRead = mSocket->readLine(block.data(), 1024);
        }

        if (bytesRead == -1)
            break;
        processCommand(block);
        if (status == Disconnected) {
            mSocket->close();
            mSocket->disconnectFromHost();
            break;
        }
    }
}

void ListenerClient::errorState()
{
    status = Error;
    sayBye();
}

void ListenerClient::processCommand(const QByteArray &cmd)
{
#ifdef LISTENERCLIENT_DEBUG
qDebug() << "<<<" << cmd;
#endif
    QString command = QString::fromUtf8(cmd).trimmed().toUpper();
    if (command == QLatin1String("GOODBYE!")) {
        status = Disconnected;
        return;
    }

    switch (status) {
    case Disconnected:
    case Error:
        break;
    case WaitingForConnect:
        if (command == QLatin1String("OK")) {
            status = Connected;
            if (mRecordingStatus == Waiting) {
                startRecording_helper();
            }
        } else {
            errorState();
        }
        break;
    case Connected:
        if (command == QLatin1String("RECORDING")) {
            fetchOperator();
        } else {
            errorState();
        }
        break;
    case StartReceiving:
        if (command.startsWith("DATA")) {
            bool conversionOK;
            dataSize = command.mid(5).trimmed().toInt(&conversionOK);
            if (!conversionOK) {
                status = Error;
                sayBye();
            } else {
                dataSize += 2; // We expect a \r\n.
                status = ReceivingData;
            }
        } else if (command == QLatin1String("STOPPED")) {
            status = Connected;
        }
        break;
    case ReceivingData:
        // Here we switch into binary mode.
        if (command == ".") {
            fetchOperator();
        } else if (dataSize > 0) {
            decodeOperator(cmd);
            dataSize = 0;
        }
        break;
    }
}


void ListenerClient::connectedToHost()
{
    status = WaitingForConnect;
}

void ListenerClient::disconnectedFromHost()
{
#ifdef LISTENERCLIENT_DEBUG
    qDebug() << "DISCONNECT";
#endif
    status = Disconnected;
    stopRecording();
}

void ListenerClient::stopRecording()
{
    if (mRecordingStatus == NotRecording)
        return;
    stopRecording_helper();
    updateRecordingStatus(NotRecording);
}

void ListenerClient::startRecording_helper()
{
    if (status == Disconnected || status == Error)
        return;
#ifdef LISTENERCLIENT_DEBUG
qDebug() << "connected";
#endif
    updateRecordingStatus(Recording);
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "start\r\n" << flush;
#ifdef LISTENERCLIENT_DEBUG
qDebug() << ">>>" << block;
#endif
    mSocket->write(block);
}

void ListenerClient::stopRecording_helper()
{
    if (status == Disconnected || status == Error)
        return;
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "STOP\r\n" << flush;
#ifdef LISTENERCLIENT_DEBUG
qDebug() << ">>>" << block;
#endif
    mSocket->write(block);
}

void ListenerClient::sayBye()
{
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "BYE\r\n" << flush;
#ifdef LISTENERCLIENT_DEBUG
qDebug() << ">>>" << block;
#endif
    mSocket->write(block);
}

void ListenerClient::fetchOperator()
{
    QByteArray block;
    QTextStream out(&block, QIODevice::WriteOnly);
    out.setCodec("UTF-8");
    out << "SEND\r\n" << flush;
#ifdef LISTENERCLIENT_DEBUG
qDebug() << ">>>" << block;
#endif
    mSocket->write(block);
    status = StartReceiving;
}

void ListenerClient::clear()
{
    qDeleteAll(klmOps);
    klmOps.clear();
}

void ListenerClient::decodeOperator(const QByteArray &block)
{
#ifdef LISTENERCLIENT_DEBUG
    qDebug() << "recevied a block";
    for (int i = 0; i < block.size(); ++i) {
        fprintf(stderr, "0x%x", block.at(i));
        if (i < block.size() - 1)
            fprintf(stderr, ",");
    }
    fprintf(stderr, "\ndone\n");
#endif
    QDataStream in(block);
    quint16 size, magic, opType;
    in >> size >> magic;
    if (magic != 0xEFEE)  // check the size too at some point
        qWarning("corrupt data");
    in >> opType;
    switch (KLMOperator::OperatorType(opType)) {
    case KLMOperator::Mental:
    case KLMOperator::Homing:
    case KLMOperator::WaitStart:
    case KLMOperator::WaitStop: {
        KLMOperator *op = new KLMOperator(KLMOperator::OperatorType(opType));
        in >> *op;
        klmOps.append(op);
        break;
    }
    case KLMOperator::Point: {
        PointOperator *op = new PointOperator(QPoint());
        in >> *op;
        klmOps.append(op);
        break;
    }
    case KLMOperator::KeyPress:
    case KLMOperator::KeyRelease: {
        KeyStrokeOperator *op = new KeyStrokeOperator(KLMOperator::OperatorType(opType), Qt::Key_Space, QString());
        in >> *op;
        klmOps.append(op);
        break;
    }
    case KLMOperator::IMComposing:
    case KLMOperator::IMCommit: {
        InputMethodOperator *op = new InputMethodOperator(KLMOperator::OperatorType(opType), QString());
        in >> *op;
        klmOps.append(op);
        break;
    }
    case KLMOperator::ButtonPress:
    case KLMOperator::ButtonRelease: {
        ButtonClickOperator *op = new ButtonClickOperator(KLMOperator::OperatorType(opType), 0);
        in >> *op;
        klmOps.append(op);
        break;
    }
    }
}

void ListenerClient::socketError(QAbstractSocket::SocketError error)
{
#ifdef LISTENERCLIENT_DEBUG
    qDebug() << "socket error" << error;
#endif
    if (status != Connected) {
#ifdef LISTENERCLIENT_DEBUG
        qDebug() << "couldn't connect!";
#endif
    }
    Q_UNUSED(error);
}
