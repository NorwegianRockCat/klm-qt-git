/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef KLMTABLEMODEL_H
#define KLMTABLEMODEL_H

#include <QtCore/QAbstractTableModel>
#include <QtCore/QList>
#include <QtXml/QtXml>

class KLMOperator;
class QUndoStack;

class KLMTableModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    enum Columns { Operator, Time, Duration, Detail, Note };
    KLMTableModel(QObject *parent = 0);
    ~KLMTableModel();

    void setKLMData(const QList<KLMOperator *> &ops);
    KLMOperator *operatorAt(const QModelIndex &index) const;
    int rowOf(KLMOperator *op) const;

    // ItemView reimplementation
    int columnCount(const QModelIndex &parent = QModelIndex()) const;
    int rowCount(const QModelIndex &parent = QModelIndex()) const;
    QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const;
    QVariant headerData(int section, Qt::Orientation orientation,
                        int role = Qt::DisplayRole) const;
    bool removeRows(int row, int count, const QModelIndex &parent = QModelIndex());
    Qt::ItemFlags flags(const QModelIndex &index) const;
    bool setData(const QModelIndex &index, const QVariant &value, int role = Qt::EditRole);

    // My own stuff.
    bool saveModel(QIODevice &saveDevice);
    bool readModel(QIODevice &readDevice);
    QString taskName() const;
    bool isDirty() const;
    void setDirty(bool newStatus);
    void guessMsByTime();
    QUndoStack *undoStack() const { return mUndoStack; }
    int timeForTask() const;

public slots:
    void setTaskName(const QString &taskName);
    void updateDirtyStatus(bool clean);
    void insertOperator(int row, KLMOperator *op);

signals:
    void taskNameChanged(const QString &newTaskName);
    void dirtyStatusChanged(bool amDirty);

private:
    void coalescePointOperators(const QList<KLMOperator *> &ops, int &ioIndex);
    void coalescePresses(const QList <KLMOperator *> &ops, int &ioIndex);
    void writeOperator(const KLMOperator *op, QXmlStreamWriter &writer) const;

    void applyHomingOperators();
    void handleMouseAsKeyboard();
    void applyMentalOperators();
    void applyMentalOperators_rule0();
    void applyMentalOperators_rule1();
    void applyMentalOperators_rule2();
    void applyMentalOperators_rule3();
    void applyMentalOperators_rule4();
    QList<KLMOperator *> operators;
    QString mTaskName;
    bool mDirty;
    QUndoStack *mUndoStack;

    friend class SetNoteCommand;
    friend class InsertOperatorCommand;
};
#endif // KLMTABLEMODEL
