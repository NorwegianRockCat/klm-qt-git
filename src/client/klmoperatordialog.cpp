/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include "klmoperatordialog.h"
#include <QtGui/QButtonGroup>
#include <QtGui/QStackedWidget>
#include <QtCore/QTimeLine>
#include <QtCore/QSettings>
#include "operator.h"

KLMOperatorDialog::KLMOperatorDialog(int row, const QTime &baseTime, int startTime, QWidget *parent)
    : QDialog(parent, Qt::Sheet), timeLine(0), mRow(row), mBaseTime(baseTime)
{
    setupUi(this);
#ifndef Q_WS_MAC
    startTimeLabel->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
    durationLabel->setAlignment(Qt::AlignVCenter | Qt::AlignLeft);
#endif
    setWindowModality(Qt::WindowModal);
    setAttribute(Qt::WA_DeleteOnClose);
    QButtonGroup *buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(mentalButton, KLMOperator::Mental);
    buttonGroup->addButton(keyButton, KLMOperator::KeyPress);
    buttonGroup->addButton(pointButton, KLMOperator::Point);
    buttonGroup->addButton(homeButton, KLMOperator::Homing);
    buttonGroup->addButton(responseButton, KLMOperator::WaitStart);
    connect(buttonGroup, SIGNAL(buttonClicked(int)), this, SLOT(operatorChoosen(int)));

    buttonGroup = new QButtonGroup(this);
    buttonGroup->addButton(leftButton, Qt::LeftButton);
    buttonGroup->addButton(middleButton, Qt::MidButton);
    buttonGroup->addButton(rightButton, Qt::RightButton);

    connect(buttonGroup, SIGNAL(buttonClicked(int)), keyCaptureLabel, SLOT(clearKey()));
    connect(stackedWidget, SIGNAL(currentChanged(int)), this, SLOT(fadeInWidget(int)));
    connect(keyCaptureLabel, SIGNAL(keySelected(int)), this, SLOT(clearMouseButtons()));

    operatorChoosen(KLMOperator::Point);
    computeCachedSize();
    operatorChoosen(KLMOperator::Mental);
    startTimeSpin->setValue(startTime);
}

KLMOperatorDialog::~KLMOperatorDialog()
{
}

void KLMOperatorDialog::clearMouseButtons()
{
    leftButton->setChecked(false);
    middleButton->setChecked(false);
    rightButton->setChecked(false);
}

void KLMOperatorDialog::done(int r)
{
    if (r == QDialog::Accepted) {
        // build an operator
        KLMOperator *op;
        QTime opTime = mBaseTime.addMSecs(startTimeSpin->value());
        if (stackedWidget->currentWidget() == mentalWidget) {
            op = new KLMOperator(KLMOperator::Mental, opTime, durationSpin->value());
        } else if (stackedWidget->currentWidget() == keyWidget) {
            if (leftButton->isChecked() || rightButton->isChecked() || middleButton->isChecked()) {
                Qt::MouseButton mouseButton;
                if (leftButton->isChecked()) {
                    mouseButton = Qt::LeftButton;
                } else if (middleButton->isChecked()) {
                    mouseButton = Qt::MidButton;
                } else {
                    mouseButton = Qt::RightButton;
                }
                op = new ButtonClickOperator(KLMOperator::ButtonRelease, mouseButton,
                                             opTime, durationSpin->value());
            } else if (keyCaptureLabel->key()) {
                // Translate the key
                op = new KeyStrokeOperator(KLMOperator::KeyRelease, keyCaptureLabel->key(),
                                           keyCaptureLabel->keyString(), opTime,
                                           durationSpin->value());
            }
        } else if (stackedWidget->currentWidget() == homingWidget) {
            op = new KLMOperator(KLMOperator::Homing, opTime, durationSpin->value());
        } else if (stackedWidget->currentWidget() == responseWidget) {
            op = new KLMOperator(KLMOperator::WaitStop, opTime,
                                 durationSpin->value());
        } else if (stackedWidget->currentWidget() == pointWidget) {
            op = new PointOperator(QPoint(startX->value(), startY->value()),
                                   QPoint(stopX->value(), stopY->value()),
                                   opTime, durationSpin->value());
        }
        emit operatorFinished(mRow, op);
    }
    QDialog::done(r);
}

void KLMOperatorDialog::operatorChoosen(int id)
{
    snapShot = QPixmap::grabWidget(stackedWidget->currentWidget());
    switch (id) {
    default:
        qWarning("Unhandled case! %d", id);
        break;
    case KLMOperator::Mental:
        stackedWidget->setCurrentWidget(mentalWidget);
        durationSpin->setValue(1350);
        break;
    case KLMOperator::KeyPress:
        stackedWidget->setCurrentWidget(keyWidget);
        durationSpin->setValue(200);
        break;
    case KLMOperator::Point:
        stackedWidget->setCurrentWidget(pointWidget);
        durationSpin->setValue(1100);
        break;
    case KLMOperator::Homing:
        stackedWidget->setCurrentWidget(homingWidget);
        durationSpin->setValue(QSettings().value(QLatin1String("H-maximum")).toInt());
        break;
    case KLMOperator::WaitStart:
        stackedWidget->setCurrentWidget(responseWidget);
        break;
    }
    if (!isVisible()) {
        layout()->activate();
        adjustSize();
        setFixedSize(size());
    } else {
        if (!timeLine) {
            timeLine = new QTimeLine(333, this);
            connect(timeLine, SIGNAL(frameChanged(int)), this, SLOT(animatedResize(int)));
        }
        QSize sh = sizeHint();
        timeLine->setFrameRange(height(), sh.height());
        timeLine->start();
        if (sh.height() > height())
            setMaximumHeight(sh.height());
        else if (sh.height() < height())
            setMinimumHeight(sh.height());
    }
}

void KLMOperatorDialog::animatedResize(int frame)
{
    if (frame == sizeHint().height())
        setFixedSize(sizeHint());
    resize(width(), frame);
}

void KLMOperatorDialog::fadeInWidget(int index)
{
    if (faderWidget)
        faderWidget->close();
    faderWidget = new FaderWidget(snapShot, stackedWidget->widget(index));
    faderWidget->start();

}

void KLMOperatorDialog::computeCachedSize()
{
    if (!cachedSize.isValid()) {
        QSizePolicy oldPolicy = stackedWidget->sizePolicy();
        QSizePolicy newPolicy = oldPolicy;
        newPolicy.setVerticalPolicy(QSizePolicy::Preferred);
        stackedWidget->setSizePolicy(newPolicy);
        cachedSize = stackedWidget->sizeHint();
        stackedWidget->setSizePolicy(oldPolicy);
    }
}

QSize KLMOperatorDialog::sizeHint() const
{
    QSize finalSize = QDialog::sizeHint();
    if (stackedWidget->currentWidget() == pointWidget || stackedWidget->currentWidget() == keyWidget) {
        finalSize.rheight() += cachedSize.height();
    }
    return finalSize;
}
