/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

#ifndef GOMSVIEW_H
#define GOMSVIEW_H

#include <QtGui/QMainWindow>
#include <QtCore/QDateTime>
#include "ui_klmqtview.h"

class KLMOperator;
class KLMTableModel;
class QMenuBar;
class QTimer;
class QUndoStack;

class KLMQtView : public QMainWindow, private Ui::KLMQtView
{
    Q_OBJECT
public:
    KLMQtView(QWidget *parent = 0);
    KLMQtView(KLMTableModel *model, QWidget *parent = 0);
    ~KLMQtView();
    void setModel(KLMTableModel *model);
    KLMTableModel *model() const;
    void setFileName(const QString &fileName);
    QString fileName() const;
signals:
    void addAvailable(bool);
    void removeAvailable(bool);
    void addNoteAvailable(bool);
    void guessMAvailable(bool);

public slots:
    void on_taskNameLineEdit_textEdited(const QString &taskName);
    void updateTaskName(const QString &name);
    bool saveModelAs();
    bool saveModel();
    void raiseAndActivateWindow();
    void on_removeOperatorButton_clicked();
    void on_addOperatorBeforeButton_clicked();
    void on_addOperatorAfterButton_clicked();
    void on_addNoteButton_clicked();
    void on_guessMButton_clicked();
    void updateButtons();
    void insertOperator(int row, KLMOperator *op);
    void printModel();
    void printModel(QPrinter *printer);
    void calcTotalTime();

protected:
    void closeEvent(QCloseEvent *event);

private:
    void init();
    void updateWindowTitle(const QString &fileName);
    bool saveModel(const QString &saveName);
    KLMTableModel *mModel;
    QString mFileName;
};

#endif // GOMSVIEW_H
