/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtCore/QSettings>
#include <QtCore/QTimer>
#include <QtGui/QCloseEvent>
#include <QtGui/QFileDialog>
#include <QtGui/QMessageBox>
#include <QtGui/QTreeWidget>
#include <QtGui/QTreeWidgetItem>
#include <QtGui/QUndoGroup>
#include <QtGui/QPageSetupDialog>

#include "bonjourservicebrowser.h"
#include "bonjourserviceresolver.h"
#include "klmqtcontroller.h"
#include "klmqtview.h"
#include "klmtablemodel.h"
#include "preferencewindow.h"
#include "klmqtapp.h"

KLMQtController::KLMQtController()
    : recordingStatus(ListenerClient::NotRecording), serviceResolver(0), mReadyToQuit(false),
      mPrefWindow(0), oldView(0)
{
    setWindowFlags(windowFlags() & ~Qt::WindowMaximizeButtonHint);
    setupUi(this);
    setStatusBar(0);
    removeToolBar(toolBar);
    delete toolBar;
    toolBar = 0;
    mRecentFilesActions = new QActionGroup(this);
    mUndoGroup = new QUndoGroup(this);
    setupMenu(menuBar());
    mListener = new ListenerClient(this);
    BonjourServiceBrowser *bonjourServiceBrowser = new BonjourServiceBrowser(this);
    cmdStartRecording->setMinimumSize(cmdStartRecording->sizeHint());
    cmdStartRecording->setEnabled(false);

    mTimer = new QTimer(this);
    clearTimeLabel();
    connect(mTimer, SIGNAL(timeout()), this, SLOT(updateTime()));
    curTime.start();  // To get that darn restart mess out of the way.
    QSettings settings;
    restoreGeometry(settings.value(QLatin1String("Geometry")).toByteArray());
    connect(mListener, SIGNAL(recordingStatusChanged(ListenerClient::RecordingStatus)),
            this, SLOT(recordingChanged(ListenerClient::RecordingStatus)));
    connect(bonjourServiceBrowser, SIGNAL(currentBonjourRecordsChanged(const QList<BonjourRecord>&)),
            this, SLOT(updateServerCombo(const QList<BonjourRecord> &)));
    updateMenu(0, this);
    bonjourServiceBrowser->browseForServiceType(QLatin1String("_klmqt._tcp"));
}

KLMQtController::~KLMQtController()
{
}

void KLMQtController::setupMenu(QMenuBar *menuBar)
{
    static bool firstTime = false;
    if (!firstTime) {
        firstTime = true;
        QAction *action;
        action = fileMenu->addAction(tr("&New"));
        action->setShortcut(QKeySequence::New);
        connect(action, SIGNAL(triggered()), this, SLOT(newModel()));
        action = fileMenu->addAction(tr("&Open..."));
        action->setShortcut(QKeySequence::Open);
        connect(action, SIGNAL(triggered()), this, SLOT(openModel()));
        QMenu *recentMenu = fileMenu->addMenu(tr("Open &Recent"));
        for (int i = 0; i < MaxRecentFiles; ++i) {
            action = new QAction(this);
            action->setVisible(false);
            connect(action, SIGNAL(triggered()), this, SLOT(openRecentModel()));
            mRecentFilesActions->addAction(action);
        }
        updateRecentFileActions();
        action = new QAction(this);
        action->setSeparator(true);
        mRecentFilesActions->addAction(action);
        action = new QAction(tr("Clear &Menu"), this);
        connect(action, SIGNAL(triggered()), this, SLOT(clearRecentFiles()));
        mRecentFilesActions->addAction(action);
        recentMenu->addActions(mRecentFilesActions->actions());
        fileMenu->addSeparator();
        closeAction = fileMenu->addAction(tr("&Close"));
        closeAction->setShortcut(QKeySequence::Close);
        connect(closeAction, SIGNAL(triggered()), this, SLOT(closeModel()));
        saveAction = fileMenu->addAction(tr("&Save"));
        saveAction->setShortcut(QKeySequence::Save);
        connect(saveAction, SIGNAL(triggered()), this, SLOT(saveModel()));
        saveAsAction = fileMenu->addAction(tr("Save &As..."));
        saveAsAction->setShortcut(QKeySequence(tr("Ctrl+Shift+S")));
        connect(saveAsAction, SIGNAL(triggered()), this, SLOT(saveModelAs()));
        fileMenu->addSeparator();
        pageSetupAction = fileMenu->addAction(tr("Pa&ge Setup..."));
#ifdef Q_WS_MAC
        pageSetupAction->setShortcut(QKeySequence(Qt::CTRL | Qt::SHIFT  + Qt::Key_P));
#endif
        connect(pageSetupAction, SIGNAL(triggered()), this, SLOT(pageSetup()));
        printAction = fileMenu->addAction(tr("&Print..."));
        printAction->setShortcut(QKeySequence::Print);
        connect(printAction, SIGNAL(triggered()), this, SLOT(printModel()));
        fileMenu->addSeparator();
        action = fileMenu->addAction(tr("&Quit"));
        action->setShortcut(QKeySequence(tr("Ctrl+Q")));
        connect(action, SIGNAL(triggered()), this, SLOT(handleQuit()));

        action = mUndoGroup->createUndoAction(this);
        action->setShortcut(QKeySequence::Undo);
        editMenu->addAction(action);
        action = mUndoGroup->createRedoAction(this);
        action->setShortcut(QKeySequence::Redo);
        editMenu->addAction(action);
        editMenu->addSeparator();
        removeOpAction = editMenu->addAction(tr("&Delete Operator"));
        removeOpAction->setShortcut(QKeySequence::Delete);
        connect(removeOpAction, SIGNAL(triggered()), this, SLOT(deleteOperator()));
        addOpBeforeAction = editMenu->addAction(tr("Add Operator &Before..."));
        connect(addOpBeforeAction, SIGNAL(triggered()), this, SLOT(addOperatorBefore()));
        addOpAfterAction = editMenu->addAction(tr("Add Operator &After..."));
        connect(addOpAfterAction, SIGNAL(triggered()), this, SLOT(addOperatorAfter()));
        addNoteAction = editMenu->addAction(tr("Edit &Note"));
        connect(addNoteAction, SIGNAL(triggered()), this, SLOT(editNote()));
        guessMAction = editMenu->addAction(tr("Compute M's"));
        connect(guessMAction, SIGNAL(triggered()), this, SLOT(computeMs()));
#ifndef Q_WS_MAC
        editMenu->addSeparator();
#endif
        action = editMenu->addAction(tr("&Preferences..."));
        action->setMenuRole(QAction::PreferencesRole);
        connect(action, SIGNAL(triggered()), this, SLOT(showPreferences()));

        minimizedAction = windowMenu->addAction(tr("&Minimize"));
        minimizedAction->setShortcut(Qt::CTRL | Qt::Key_M);
        connect(minimizedAction, SIGNAL(triggered()), this, SLOT(minimizeCurrentWindow()));
        zoomAction = windowMenu->addAction(tr("&Zoom"));
        connect(zoomAction, SIGNAL(triggered()), this, SLOT(zoomCurrentWindow()));
        windowMenu->addSeparator();
        bringToFrontAction = windowMenu->addAction(tr("&Bring All to Front"));
        connect(bringToFrontAction, SIGNAL(triggered()), this, SLOT(bringAllToFront()));
        connect(windowMenu, SIGNAL(aboutToShow()), this, SLOT(generateWindowList()));
        connect(qApp, SIGNAL(focusChanged(QWidget *, QWidget *)), this, SLOT(updateMenu(QWidget *, QWidget *)));
#ifdef Q_WS_MAC
        // set a generic menubar for windows with no menubars.
        QMenuBar *genericMenuBar = new QMenuBar(0);
        setupMenu(genericMenuBar);
#endif
    } else {
        // Create a fact similie menubar.
        QList<QAction *> actions = this->menuBar()->actions();
        foreach (QAction *action, actions) {
            QMenu *menuToCopy = action->menu();
            QMenu *newMenu = menuBar->addMenu(menuToCopy->title());
            addActionsToMenu(newMenu, menuToCopy->actions());
        }
    }
}

void KLMQtController::addActionsToMenu(QMenu *menu, QList<QAction *> actions)
{
    foreach (QAction *action, actions) {
        if (QMenu *actionMenu = action->menu()) {
            QMenu *newMenu = menu->addMenu(actionMenu->title());
            addActionsToMenu(newMenu, actionMenu->actions());
            continue;
        }
        menu->addAction(action);
    }
}

void KLMQtController::showPreferences()
{
    if (!mPrefWindow) {
        mPrefWindow = new PreferenceWindow(this);
        connect(mPrefWindow, SIGNAL(destroyed()), this, SLOT(cleanupPreferences()));
    }
    mPrefWindow->show();
    mPrefWindow->activateWindow();
}

void KLMQtController::cleanupPreferences()
{
    mPrefWindow = 0;
}

void KLMQtController::updateMenu(QWidget *oldWidget, QWidget *newWidget)
{
    QWidget *oldWindow = oldWidget ? oldWidget->window() : 0;
    QWidget *window = newWidget ? newWidget->window() : 0;
    if (oldWindow == window)
        return;

    if (oldView) {
        disconnect(oldView, SIGNAL(addAvailable(bool)), addOpAfterAction, SLOT(setEnabled(bool)));
        disconnect(oldView, SIGNAL(addAvailable(bool)), addOpBeforeAction, SLOT(setEnabled(bool)));
        disconnect(oldView, SIGNAL(removeAvailable(bool)), removeOpAction, SLOT(setEnabled(bool)));
        disconnect(oldView, SIGNAL(addNoteAvailable(bool)), addNoteAction, SLOT(setEnabled(bool)));
        disconnect(oldView, SIGNAL(guessMAvailable(bool)), guessMAction, SLOT(setEnabled(bool)));
    }
    addOpAfterAction->setEnabled(false);
    addOpBeforeAction->setEnabled(false);
    removeOpAction->setEnabled(false);
    addNoteAction->setEnabled(false);
    guessMAction->setEnabled(false);
    
    KLMQtView *view = qobject_cast<KLMQtView *>(window);
    if (mPrefWindow == window) {
        closeAction->setEnabled(true);
    } else if (view) {
        Qt::WindowStates windowState = view->windowState();
        Qt::WindowFlags windowFlags = view->windowFlags();
        zoomAction->setEnabled(!(windowState & Qt::WindowMaximized)
                               && (windowFlags & Qt::WindowMaximizeButtonHint));
        minimizedAction->setEnabled(!(windowState & Qt::WindowMinimized)
                                    && (windowFlags & Qt::WindowMinimizeButtonHint));
        saveAction->setEnabled(true);
        saveAsAction->setEnabled(true);
        closeAction->setEnabled(true);
        printAction->setEnabled(true);
        connect(view, SIGNAL(addAvailable(bool)), addOpAfterAction, SLOT(setEnabled(bool)));
        connect(view, SIGNAL(addAvailable(bool)), addOpBeforeAction, SLOT(setEnabled(bool)));
        connect(view, SIGNAL(removeAvailable(bool)), removeOpAction, SLOT(setEnabled(bool)));
        connect(view, SIGNAL(addNoteAvailable(bool)), addNoteAction, SLOT(setEnabled(bool)));
        connect(view, SIGNAL(guessMAvailable(bool)), guessMAction, SLOT(setEnabled(bool)));
        view->updateButtons();
    } else {
        saveAction->setEnabled(false);
        saveAsAction->setEnabled(false);
        printAction->setEnabled(false);
        closeAction->setEnabled(false);
        zoomAction->setEnabled(false);
        minimizedAction->setEnabled(false);
    }
    updateUndoStack(view);
    oldView = view;
}

void KLMQtController::bringAllToFront()
{
    foreach (QWidget *widget, QApplication::topLevelWidgets()) {
        widget->raise();
    }
}

void KLMQtController::minimizeCurrentWindow()
{
    if (QWidget *widget = QApplication::activeWindow()) {
        widget->showMinimized();
        updateMenu(0, QApplication::activeWindow());
    }
}

void KLMQtController::zoomCurrentWindow()
{
    if (QWidget *widget = QApplication::activeWindow()) {
        widget->showMaximized();
        updateMenu(0, QApplication::activeWindow());
    }
}

void KLMQtController::generateWindowList()
{
    // Clear all the old actions
    QList<QAction *> actions = windowMenu->actions();
    int bringToFrontIndex = actions.indexOf(bringToFrontAction);
    if (bringToFrontIndex != -1) {
        for (int i = bringToFrontIndex + 1; i < actions.count(); ++i) {
            delete actions.at(i);
        }
    }
    QList<QWidget *> windows = QApplication::topLevelWidgets();
    if (!windows.isEmpty()) {
        windowMenu->addSeparator();
        foreach (QWidget *window, windows) {
            if (qobject_cast<KLMQtController *>(window) || qobject_cast<KLMQtView *>(window)) {
                QAction *action = windowMenu->addAction(window->windowTitle());
                action->setCheckable(true);
                connect(action, SIGNAL(triggered()), window, SLOT(raiseAndActivateWindow()));
                if (window->isActiveWindow()) {
                    action->setChecked(true);
                }
            }
        }
    }
}

void KLMQtController::closeModel()
{
    QWidget *widget = QApplication::activeWindow();
    if (KLMQtView *view = activeKLMView()) {
        view->close();
    } else if (widget == mPrefWindow) {
        mPrefWindow->close();
    }
}

void KLMQtController::saveModelAs()
{
    if (KLMQtView *view = activeKLMView()) {
        view->saveModelAs();
    }
}

void KLMQtController::saveModel()
{
    if (KLMQtView *view = activeKLMView()) {
        if (view->saveModel()) {
            addRecentFile(view->fileName());
        }
    }
}

KLMQtView *KLMQtController::createKLMView(KLMTableModel *model)
{
    KLMQtView *view;
    if (model) {
        view = new KLMQtView(model);
    } else {
        view = new KLMQtView();
        model = view->model();
    }
    // Ideally I wouldn't do this, but since each window is a main window, I can at least
    // exercise the code this way.
    setupMenu(view->menuBar());
    mUndoGroup->addStack(model->undoStack());
    view->setAttribute(Qt::WA_DeleteOnClose);
    connect(view, SIGNAL(destroyed()), this, SLOT(removeViewFromList()));
    mList.append(view);
    return view;
}

void KLMQtController::newModel()
{
    KLMQtView *view = createKLMView();
    view->show();
}

void KLMQtController::openModel()
{
    QString openName = QFileDialog::getOpenFileName(this, QString(), QString(),
                                                    QLatin1String("*.klm"), 0,
                                                    QFileDialog::DontUseSheet);
    if (openName.isEmpty())
        return;

    openModel(openName);
}

bool KLMQtController::openModel(const QString &openName)
{
    // Make sure that we don't open the same file twice!
    foreach (KLMQtView *view, mList) {
        if (view->fileName() == openName) {
            view->raiseAndActivateWindow();
            addRecentFile(openName);
            return false;
        }
    }

    QFile file(openName);
    bool openOK = file.open(QIODevice::ReadOnly);
    if (!openOK) {
        QMessageBox messageBox(QMessageBox::Critical, tr("KLM Client"),
                               tr("Unable to open file"),
                               QMessageBox::Retry | QMessageBox::Cancel,
                               this);
        messageBox.setWindowModality(Qt::WindowModal);
        static_cast<QPushButton *>(messageBox.button(QMessageBox::Retry))->setDefault(true);
        while (!openOK) {
            messageBox.setInformativeText(tr("The operating system reported "
                        "this problem: %1. You can try to open the file again,"
                        "or cancel the operation.").arg(file.errorString()));
            switch (messageBox.exec()) {
            case QMessageBox::Retry:
                openOK = file.open(QIODevice::ReadOnly);
                break;
            case QMessageBox::Cancel:
                return false;
            }
        }
    }
    KLMTableModel *model = new KLMTableModel();
    if (model->readModel(file)) {
        KLMQtView *view = createKLMView(model);
        view->setFileName(openName);
        view->show();
        addRecentFile(openName);
    }
    return true;
}

void KLMQtController::updateServerCombo(const QList<BonjourRecord> &list)
{
    serverView->clear();
    foreach (BonjourRecord record, list) {
        QVariant variant;
        variant.setValue(record);
        QTreeWidgetItem *processItem = new QTreeWidgetItem(serverView, QStringList()
                                                                           << record.serviceName);
        processItem->setData(0, Qt::UserRole, variant);
    }

    if (serverView->invisibleRootItem()->childCount() > 0) {
        serverView->invisibleRootItem()->child(0)->setSelected(true);
    }
    cmdStartRecording->setEnabled(serverView->invisibleRootItem()->childCount() != 0);
}

void KLMQtController::on_cmdStartRecording_clicked()
{
    if (recordingStatus != ListenerClient::NotRecording) {
        mListener->stopRecording();
    } else {
        QList<QTreeWidgetItem *> selectedItems = serverView->selectedItems();
        if (selectedItems.isEmpty())
            return;

        if (!serviceResolver) {
            serviceResolver = new BonjourServiceResolver(this);
            connect(serviceResolver, SIGNAL(bonjourRecordResolved(const QHostInfo &, int)),
                    mListener, SLOT(connectToServer(const QHostInfo &, int)));
        }
        mListener->tryDisconnect();
        QTreeWidgetItem *item = selectedItems.at(0);
        QVariant variant = item->data(0, Qt::UserRole);
        serviceResolver->resolveBonjourRecord(variant.value<BonjourRecord>());
    }
}

void KLMQtController::recordingChanged(ListenerClient::RecordingStatus newStatus)
{
    if (newStatus == recordingStatus)
        return;

    recordingStatus = newStatus;
    switch (recordingStatus) {
    case ListenerClient::Waiting:
        mListener->clear();
        clearTimeLabel();
        cmdStartRecording->setText(tr("Connecting..."));
        break;
    case ListenerClient::NotRecording: {
        cmdStartRecording->setText(tr("Start Recording"));
        KLMTableModel *model = new KLMTableModel();
        model->setKLMData(mListener->klmOperators());
        KLMQtView *view = new KLMQtView(model);
        mUndoGroup->addStack(model->undoStack());
        view->setAttribute(Qt::WA_DeleteOnClose);
        connect(view, SIGNAL(destroyed()), this, SLOT(removeViewFromList()));
        mList.append(view);
        view->show();
        mTimer->stop();
        updateTime();
        break; }
    case ListenerClient::Recording:
        cmdStartRecording->setText(tr("Stop Recording"));
        curTime.restart();
        mTimer->start(33);
        break;
    }
}

void KLMQtController::clearTimeLabel()
{
    timeLabel->setText("00000");
}


void KLMQtController::updateTime()
{
    timeLabel->setText(QString::number(curTime.elapsed()));
}


void KLMQtController::raiseAndActivateWindow()
{
    raise();
    activateWindow();
}

void KLMQtController::removeViewFromList()
{
    KLMQtView *deletedView = static_cast<KLMQtView *>(sender());
    mList.removeAll(deletedView);
    updateMenu(0, QApplication::activeWindow());
}

bool KLMQtController::handleClose()
{
    if (mReadyToQuit)
        return true;

    QList<KLMQtView *> dirtyModels;
    foreach (KLMQtView *w, mList) {
        if (w->model()->isDirty())
            dirtyModels.append(w);
    }

    if (!dirtyModels.isEmpty()) {
        if (dirtyModels.size() == 1) {
            KLMQtView *view = dirtyModels.at(0);
            view->raiseAndActivateWindow();
            if (!view->close())
                return false;
        } else {
            QMessageBox box(QMessageBox::Warning, tr("Save Forms?"),
                    tr("There are %1 forms with unsaved changes."
                        " Do you want to review these changes before quitting?")
                    .arg(dirtyModels.size()),
                    QMessageBox::Cancel | QMessageBox::Discard | QMessageBox::Save);
            box.setInformativeText(tr("If you don't review your documents, all your changes will"
                                      " be lost."));
            box.button(QMessageBox::Discard)->setText(tr("Discard Changes"));
            QPushButton *save = static_cast<QPushButton *>(box.button(QMessageBox::Save));
            save->setText(tr("Review Changes"));
            box.setDefaultButton(save);
            switch (box.exec()) {
            case QMessageBox::Cancel:
                return false;
            case QMessageBox::Save:
               foreach (KLMQtView *w, dirtyModels) {
                   w->show();
                   w->raise();
                   if (!w->close())
                       return false;
               }
               break;
            case QMessageBox::Discard:
              foreach (KLMQtView *w, dirtyModels) {
                  w->model()->setDirty(false);
                  w->setWindowModified(false);
              }
              break;
            }
        }
    }

    foreach (KLMQtView *w, mList) {
        w->raiseAndActivateWindow();
        w->close();
    }

    // Spare us running this twice.
    mReadyToQuit = true;
    return mReadyToQuit;
}

void KLMQtController::handleQuit()
{
    if (handleClose())
        QMetaObject::invokeMethod(qApp, "quit", Qt::QueuedConnection);
}

void KLMQtController::closeEvent(QCloseEvent *event)
{
    event->setAccepted(handleClose());
}

void KLMQtController::updateUndoStack(KLMQtView *view)
{
    mUndoGroup->setActiveStack(view ? view->model()->undoStack() : 0);
}

void KLMQtController::addRecentFile(const QString &fileName)
{
    QSettings settings;
    QStringList files = settings.value(QLatin1String("recentFilesList")).toStringList();
    files.removeAll(fileName);
    files.prepend(fileName);
    while (files.size() > MaxRecentFiles)
        files.removeLast();

    settings.setValue(QLatin1String("recentFilesList"), files);
    updateRecentFileActions();
}


void KLMQtController::updateRecentFileActions()
{
    QSettings settings;
    QStringList files = settings.value(QLatin1String("recentFilesList")).toStringList();
    const int OriginalSize = files.size();
    int numRecentFiles = qMin(OriginalSize, int(MaxRecentFiles));
    const QList<QAction *> recentFilesActs = mRecentFilesActions->actions();

    for (int i = 0; i < numRecentFiles; ++i) {
        const QFileInfo fi(files.at(i));
        if (!fi.exists()) {
            files.removeAt(i);
            --i;
            numRecentFiles = qMin(files.size(), int(MaxRecentFiles));
            continue;
        }
        const QString text = fi.fileName();
        recentFilesActs[i]->setText(text);
        recentFilesActs[i]->setIconText(files.at(i));
        recentFilesActs[i]->setVisible(true);
    }

    for (int j = numRecentFiles; j < MaxRecentFiles; ++j)
        recentFilesActs[j]->setVisible(false);

    if (OriginalSize != files.size())
        settings.setValue(QLatin1String("recentFilesList"), files);
}

void KLMQtController::openRecentModel()
{
    if (const QAction *action = qobject_cast<const QAction *>(sender())) {
        if (!openModel(action->iconText()))
            updateRecentFileActions();  // Doesn't exist, remove it from the settings.
    }
}

void KLMQtController::clearRecentFiles()
{
    QSettings settings;
    settings.setValue(QLatin1String("recentFilesList"), QStringList());
    updateRecentFileActions();
}

KLMQtView *KLMQtController::activeKLMView() const
{
    return qobject_cast<KLMQtView *>(QApplication::activeWindow());
}

void KLMQtController::deleteOperator()
{
    if (KLMQtView *view = activeKLMView())
        view->on_removeOperatorButton_clicked();
}

void KLMQtController::addOperatorBefore()
{
    if (KLMQtView *view = activeKLMView())
        view->on_addOperatorBeforeButton_clicked();
}

void KLMQtController::addOperatorAfter()
{
    if (KLMQtView *view = activeKLMView())
        view->on_addOperatorAfterButton_clicked();
}

void KLMQtController::editNote()
{
    if (KLMQtView *view = activeKLMView())
        view->on_addNoteButton_clicked();
}

void KLMQtController::computeMs()
{
    if (KLMQtView *view = activeKLMView())
        view->on_guessMButton_clicked();    
}

void KLMQtController::pageSetup()
{
    QPageSetupDialog pageSetupDialog(KLMQtApplication::printer());
    pageSetupDialog.exec();
}

void KLMQtController::printModel()
{
    if (KLMQtView *view = activeKLMView())
        view->printModel();  
}
