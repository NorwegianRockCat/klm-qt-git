/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtCore/QTime>
#include <QtCore/QVariant>
#include <QtCore/QSettings>
#include <QtGui/QMessageBox>
#include <QtGui/QUndoCommand>
#include <QtGui/QUndoStack>
#include <QtXml/QXmlStreamReader>
#include "klmqtapp.h"
#include "klmtablemodel.h"
#include "operator.h"
#include "klmmlreader.h"

KLMTableModel::KLMTableModel(QObject *parent)
    : QAbstractTableModel(parent), mDirty(false)
{
    mUndoStack = new QUndoStack(this);
    connect(mUndoStack, SIGNAL(cleanChanged(bool)), this, SLOT(updateDirtyStatus(bool)));
}

KLMTableModel::~KLMTableModel()
{
    mUndoStack->clear(); // Will ensure we don't get double deletes.
}

int KLMTableModel::columnCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return 5;
}

int KLMTableModel::rowCount(const QModelIndex &parent) const
{
    if (parent.isValid())
        return 0;
    return operators.size();
}

QVariant KLMTableModel::data(const QModelIndex &index, int role) const
{
    if (!index.isValid())
        return QVariant();

    QVariant returnMe;
    const KLMOperator *op = operators.at(index.row());
    int column = index.column();
    if (role == Qt::DisplayRole) {
        if (column == Operator || column == Detail) {
            switch (op->type()) {
            case KLMOperator::Homing:
            case KLMOperator::Mental:
            case KLMOperator::Point:
                if (column == Operator) {
                    switch (op->type()) {
                    default:
                        break;
                    case KLMOperator::Homing:
                        returnMe = QLatin1String("H");
                        break;
                    case KLMOperator::Mental:
                        returnMe = QLatin1String("M");
                        break;
                    case KLMOperator::Point:
                        returnMe = QLatin1String("P");
                        break;
                    }
                } else {
                    returnMe = QLatin1String("");
                }
                break;
            case KLMOperator::IMComposing:
            case KLMOperator::IMCommit:
                if (column == Operator) {
                    returnMe = QLatin1String("I");
                } else if (column == Detail) {
                    const InputMethodOperator *input = static_cast<const InputMethodOperator *>(op);
                    returnMe = tr("%1 (%2)").arg(input->type() == KLMOperator::IMComposing
                                                                ? QLatin1String("IM Compose")
                                                                : QLatin1String("IMCommit"))
                                            .arg(input->text());
                }
                break;
            case KLMOperator::KeyPress:
            case KLMOperator::KeyRelease:
                if (column == Operator) {
                    returnMe = QLatin1String("K");
                } else if (column == Detail) {
                    const KeyStrokeOperator *st
                                        = static_cast<const KeyStrokeOperator *>(op);
                    QString keyString = KLMQtApplication::keyToString(st->key());
                    if (keyString.isEmpty())
                        keyString = st->text();
                    returnMe = keyString;
                }
                break;
            case KLMOperator::ButtonPress:
            case KLMOperator::ButtonRelease:
                if (column == Operator) {
                    returnMe = QLatin1String("K");
                } else if (column == Detail) {
                    const ButtonClickOperator *bc
                                    = static_cast<const ButtonClickOperator *>(op);
                    returnMe = tr("%1 (%2)").arg(bc->type()
                                                    == KLMOperator::ButtonPress
                                             ? QLatin1String("Mouse Button Press")
                                             : QLatin1String("Mouse Button Release"))
                                            .arg(bc->buttons());
                }
                break;
            case KLMOperator::WaitStart:
            case KLMOperator::WaitStop:
                if (column == Operator)
                    returnMe = QLatin1String("W");
                break;
            }
        }
        if (column == Time) {
            const KLMOperator *startPoint = operators.at(0);
            returnMe = startPoint->operatorTime().msecsTo(op->operatorTime());
        } else if (column == Duration) {
            returnMe = op->duration();
        } else if (column == Note) {
            returnMe = op->note();
        }
    } else if (role == Qt::TextAlignmentRole) {
        if (column == Time || column == Duration)
            returnMe = int(Qt::AlignVCenter | Qt::AlignRight);
        else
            returnMe = Qt::AlignCenter;
    }
    return returnMe;
}

void KLMTableModel::setKLMData(const QList<KLMOperator *> &ops)
{
    qDeleteAll(operators);
    operators.clear();
    for (int i = 0; i < ops.size(); ++i) {
        const KLMOperator *op = ops.at(i);
        switch (op->type()) {
        case KLMOperator::Mental:
        case KLMOperator::WaitStart:
        case KLMOperator::WaitStop:
        case KLMOperator::Homing:
            operators.append(new KLMOperator(*op));
            break;
        case KLMOperator::Point:
            coalescePointOperators(ops, i);
            break;
        case KLMOperator::ButtonPress:
        case KLMOperator::KeyPress:
            coalescePresses(ops, i);
            break;
        case KLMOperator::ButtonRelease:
            operators.append(new ButtonClickOperator(static_cast<const ButtonClickOperator &>(*op)));
            break;
        case KLMOperator::KeyRelease:
            operators.append(new KeyStrokeOperator(static_cast<const KeyStrokeOperator &>(*op)));
            break;
        case KLMOperator::IMComposing:
        case KLMOperator::IMCommit:
            operators.append(new InputMethodOperator(static_cast<const InputMethodOperator &>(*op)));
            break;
        }
    }
    // post process
    applyHomingOperators();
    handleMouseAsKeyboard();
    applyMentalOperators();

    // Update the view
    beginInsertRows(QModelIndex(), 0, operators.size() - 1);
    endInsertRows();
    setDirty(true);
}

void KLMTableModel::handleMouseAsKeyboard()
{
    // This is for mobile phones (like the Neo 1973) that don't have a
    // keyboard, but may use mouse clicks to generate keyboard events. These
    // follow a certain pattern. Of a Mouse Press, then a very quick H then a
    // very, very quick K for a keyboard. We try to turn this into a keyboard K
    // that starts with the mouse press and ends with the key release.
    for (int i = 0; i < operators.size(); ++i) {
        KLMOperator *op = operators.at(i);
        if (op->type() == KLMOperator::ButtonPress || op->type() == KLMOperator::ButtonRelease) {
            if (i + 2 < operators.size()) {
                KLMOperator *hOp = operators.at(i + 1);
                if (hOp->type() == KLMOperator::Homing) {
                    KeyStrokeOperator *kOp
                        = dynamic_cast<KeyStrokeOperator *>(operators.at(i + 2));
                    if (kOp
                            && (kOp->type() == KLMOperator::KeyPress
                                || kOp->type() == KLMOperator::KeyRelease)
                            && hOp->duration() <= 128 && kOp->duration() < 24) {
                        QTime t = op->operatorTime();
                        t = t.addMSecs(op->duration());
                        t = t.addMSecs(t.msecsTo(hOp->operatorTime()) + hOp->duration());
                        t = t.addMSecs(t.msecsTo(kOp->operatorTime()) + kOp->duration());
                        kOp->setOperatorTime(op->operatorTime());
                        kOp->setDuration(kOp->operatorTime().msecsTo(t));
                        operators[i] = kOp;
                        operators.removeAt(i + 1);
                        operators.removeAt(i + 1);
                        if (i + 1 < operators.size()
                                && operators.at(i + 1)->type() == KLMOperator::Homing) {
                            operators.removeAt(i + 1);
                        }
                    }
                }
            }
        }
    }
}

void KLMTableModel::coalescePointOperators(const QList<KLMOperator *> &ops,
                                           int &ioIndex)
{
    // Coalesce all point operators between ioIndex and the next non-point op.
    // Return the final index in ioIndex
    KLMOperator *op = ops.at(ioIndex);
    if (op->type() == KLMOperator::Point) {
        const PointOperator *point = static_cast<const PointOperator *>(op);
        const PointOperator *lastPoint = point;
        int j = ioIndex;
        while (j < ops.size() && ops.at(j)->type() == KLMOperator::Point)
            lastPoint = static_cast<const PointOperator *>(ops.at(j++));

        ioIndex = j > ioIndex ? j - 1 : ioIndex;
        operators.append(new PointOperator(point->location(),
                    lastPoint->location(), point->operatorTime(),
                    point->operatorTime().msecsTo(lastPoint->operatorTime())));
    }
}

void KLMTableModel::coalescePresses(const QList <KLMOperator *> &ops, int &ioIndex)
{

    KLMOperator *op = ops.at(ioIndex);
    if (op->type() == KLMOperator::KeyPress
            || op->type() == KLMOperator::ButtonPress) {
        const KeyStrokeOperator *keyStroke = 0;
        const ButtonClickOperator *buttonClick = 0;
        if (op->type() == KLMOperator::KeyPress) {
            keyStroke = static_cast<const KeyStrokeOperator *>(op);
        } else {
            buttonClick = static_cast<const ButtonClickOperator *>(op);
        }
        int j = ioIndex;
        if (++j < ops.size()) {
            if (keyStroke) {
                if (const KeyStrokeOperator *nextOp
                          = dynamic_cast<KeyStrokeOperator *>(ops.at(j))) {
                    if (nextOp->type() == KLMOperator::KeyRelease
                            && nextOp->key() == keyStroke->key()) {
                        operators.append(new KeyStrokeOperator(KLMOperator::KeyPress,
                                    keyStroke->key(), keyStroke->text(),
                                    keyStroke->operatorTime(),
                                    keyStroke->operatorTime()
                                            .msecsTo(nextOp->operatorTime())));
                        ioIndex = j;
                    }
                }
                if (ioIndex != j) {
                    // We couldn't find a mate, just add it.
                    operators.append(new KeyStrokeOperator(*keyStroke));
                }
            } else { // buttonClick != 0
                if (const ButtonClickOperator *nextOp
                        = dynamic_cast<ButtonClickOperator *>(ops.at(j))) {
                    if (nextOp->type() == KLMOperator::ButtonRelease
                            && nextOp->buttons() == buttonClick->buttons()) {
                        operators.append(new ButtonClickOperator(KLMOperator::ButtonPress,
                                    buttonClick->buttons(),
                                    buttonClick->operatorTime(),
                                    buttonClick->operatorTime()
                                            .msecsTo(nextOp->operatorTime())));
                        ioIndex = j;
                    }
                }
                if (ioIndex != j) {
                    // We couldn't find a mate, just add it.
                    operators.append(new ButtonClickOperator(*buttonClick));
                }
            }
        }
    }
}

void KLMTableModel::applyHomingOperators()
{
    if (operators.size() < 1)
        return; // Nothing to figure out
    QSettings settings;
    const int HomingTime = settings.value(QLatin1String("H-maximum"), 400).toInt();
    // Attempt to place homing operators between mouse and keyboard operators
    bool onKeyboard = false;
    bool onMouse = false;
    const KLMOperator *op = operators.at(0);
    switch (op->type()) {
    case KLMOperator::KeyPress:
    case KLMOperator::KeyRelease:
        onKeyboard = true;
        break;
    case KLMOperator::ButtonPress:
    case KLMOperator::ButtonRelease:
    case KLMOperator::Point:
        onMouse = true;
        break;
    default:
        break;
    }

    for (int i = 1; i < operators.size(); ++i) {
        const KLMOperator *prevOp = op;
        bool needHoming = false;
        op = operators.at(i);
        switch (op->type()) {
        case KLMOperator::KeyPress:
        case KLMOperator::KeyRelease:
            if (!onKeyboard) {
                onMouse = false;
                needHoming = onKeyboard = true;
            }
            break;
        case KLMOperator::ButtonPress:
        case KLMOperator::ButtonRelease:
        case KLMOperator::Point:
            if (!onMouse) {
                onKeyboard = false;
                needHoming = onMouse = true;
            }
            break;
        default:
            break;
        }
        if (needHoming) {
            QTime hStart = prevOp->operatorTime().addMSecs(prevOp->duration());
            operators.insert(i, new KLMOperator(KLMOperator::Homing, hStart,
                                             qMin(HomingTime, hStart.msecsTo(op->operatorTime()))));
            ++i; // No need to look at this op again.
        }
    }
    if (settings.value(QLatin1String("auto-add-H"), true).toInt()) {
        operators.prepend(new KLMOperator(KLMOperator::Homing,
                          operators.at(0)->operatorTime().addMSecs(-HomingTime), HomingTime));
    }
}

QVariant KLMTableModel::headerData(int section, Qt::Orientation orientation,
                                   int role) const
{
    QVariant var;
    if (orientation != Qt::Horizontal || role != Qt::DisplayRole) {
        var = QAbstractTableModel::headerData(section, orientation, role);
    } else {
        switch (section) {
        case Operator:
            var = tr("Operator");
            break;
        case Time:
            var = tr("Elapsed (msec)");
            break;
        case Duration:
            var = tr("Duration (msec)");
            break;
        case Detail:
            var = tr("Detail");
            break;
        case Note:
            var = tr("Note");
            break;
        }
    }
    return var;

}

void KLMTableModel::applyMentalOperators()
{
    return;
    // Just apply all the rules in sequence.
    applyMentalOperators_rule0();
    applyMentalOperators_rule1();
    applyMentalOperators_rule2();
    applyMentalOperators_rule3();
    applyMentalOperators_rule4();
}

void KLMTableModel::applyMentalOperators_rule0()
{
    // Insert M's in front of all K's that are not part of argument strings
    // proper (e.g., text or numbers). Place M's in front of all P's that
    // select commands (not arguments).
    bool needMental;
    for (int i = 0; i < operators.size(); ++i) {
        const KLMOperator *op = operators.at(i);
        needMental = false;
        if (op->type() == KLMOperator::KeyPress) {
            if (i == 0) {
                needMental = true;
            } else {
                // Look back and see if what something was typed previously...
                // This indicates that it's an argument and not a command.
                // ### Need to look at key to determine what is a command or not.
                const KLMOperator *prevOp = operators.at(i - 1);
                needMental = (prevOp->type() != KLMOperator::KeyPress
                              && prevOp->type() != KLMOperator::KeyRelease);

            }
        } else if (op->type() == KLMOperator::ButtonPress) {

            // In general a button press happens to select a command, but a
            // second button click is anticipated for a double click, so only
            // add a mental operator for button presses that come after moves.

            // ### Consider filtering out very, small movements... they might
            // be an argument after all (e.g. click two points to select an area).
            if (i == 0) {
                needMental = true;
            } else {
                const KLMOperator *prevOp = operators.at(i - 1);
                needMental = (prevOp->type() == KLMOperator::Point);
            }
        } else if (op->type() == KLMOperator::Point) {
            // Points usually go to commands and not arguments. Since we
            // compressed all the points, we can just add the operator.
            needMental = true;
        }

        if (needMental) {
            QTime dt = op->operatorTime().addMSecs(-1350);
            operators.insert(i,
                    new KLMOperator(KLMOperator::Mental, dt, 1350));
            ++i; // No need to look at this op again.
        }
    }
}

void KLMTableModel::applyMentalOperators_rule1()
{

    // If an operator following an M is /fully anticipated/ in an operator just
    // previous an M, then delete the M (e.g., PMK -> PK).

    // ### Currently only look for PMK, can't think of anything else that this
    // fixes up.

    // Since we are pretty much only are looking at what is in the middle,
    // we can safely just check what is between everything.
    for (int i = 1; i < operators.size() - 1; ++i) {
        const KLMOperator *op = operators.at(i);
        if (op->type() == KLMOperator::Mental) {
            const KLMOperator *prev = operators.at(i - 1);
            if (prev->type() == KLMOperator::Point) {
                const KLMOperator *next = operators.at(i + 1);
                if (next->type() == KLMOperator::KeyPress
                        || next->type() == KLMOperator::ButtonPress) {
                    operators.removeAt(i);
                    // now next is at i, so it is safe to just continue.
                }
            }
        }
    }
}

void KLMTableModel::applyMentalOperators_rule2()
{
}

void KLMTableModel::applyMentalOperators_rule3()
{
}

void KLMTableModel::applyMentalOperators_rule4()
{
}

// It's assumed here that the device was opened for write already.
bool KLMTableModel::saveModel(QIODevice &saveDevice)
{
    QByteArray ba;
    QXmlStreamWriter writer(&ba);
    writer.setAutoFormatting(true);
    writer.writeStartDocument();
    writer.writeDTD("<!DOCTYPE klmML SYSTEM \"klmml.dtd\">");
    writer.writeStartElement(QLatin1String("klmML"));
    writer.writeAttribute(QLatin1String("klmVersion"), QString::number(KLMMLReader::klmMLVersion()));
    writer.writeStartElement(QLatin1String("klmList"));
    if (!mTaskName.isEmpty())
        writer.writeAttribute(QLatin1String("task"), mTaskName);

    for (int i = 0; i < operators.size(); ++i) {
        writeOperator(operators.at(i), writer);
    }
    writer.writeEndDocument();

    bool saveOK = (saveDevice.write(ba) == ba.size());
    setDirty(!saveOK);
    return saveOK;
}

void KLMTableModel::writeOperator(const KLMOperator *op,
                                  QXmlStreamWriter &writer) const
{
    writer.writeStartElement(QLatin1String("Operator"));
    writer.writeTextElement(QLatin1String("type"), QString::number(op->type()));
    writer.writeStartElement(QLatin1String("operatorTime"));
    writer.writeTextElement(QLatin1String("Hour"), QString::number(op->operatorTime().hour()));
    writer.writeTextElement(QLatin1String("Minute"), QString::number(op->operatorTime().minute()));
    writer.writeTextElement(QLatin1String("Second"), QString::number(op->operatorTime().second()));
    writer.writeTextElement(QLatin1String("Millisecond"), QString::number(op->operatorTime().msec()));
    writer.writeEndElement();
    writer.writeTextElement(QLatin1String("duration"), QString::number(op->duration()));
    if (!op->note().isEmpty()) {
        writer.writeTextElement(QLatin1String("note"), op->note());
    }
    switch (op->type()) {
    case KLMOperator::Point: {
        const PointOperator *point = static_cast<const PointOperator *>(op);
        writer.writeStartElement(QLatin1String("location"));
        writer.writeTextElement(QLatin1String("x"), QString::number(point->location().x()));
        writer.writeTextElement(QLatin1String("y"), QString::number(point->location().y()));
        writer.writeEndElement();
        if (!point->stopLocation().isNull()) {
            writer.writeStartElement(QLatin1String("stopLocation"));
            writer.writeTextElement(QLatin1String("x"), QString::number(point->stopLocation().x()));
            writer.writeTextElement(QLatin1String("y"), QString::number(point->stopLocation().y()));
            writer.writeEndElement();
        }
        break;
    }
    case KLMOperator::ButtonPress:
    case KLMOperator::ButtonRelease: {
        const ButtonClickOperator *bco = static_cast<const ButtonClickOperator *>(op);
        writer.writeTextElement(QLatin1String("buttons"), QString::number(bco->buttons()));
        break;
    }
    case KLMOperator::KeyPress:
    case KLMOperator::KeyRelease: {
        const KeyStrokeOperator *kso = static_cast<const KeyStrokeOperator *>(op);
        writer.writeTextElement(QLatin1String("key"), QString::number(kso->key()));
        QString text = KLMQtApplication::keyToString(kso->key());
        // DON'T write the text if we get a string converted here, we can just do it when
        // it's loaded.
        if (text.isEmpty()) {
            text = kso->text();
            if (!text.isEmpty())
                writer.writeTextElement(QLatin1String("string"), text);
        }
        break;
    }
    case KLMOperator::IMComposing:
    case KLMOperator::IMCommit: {
        const InputMethodOperator *input = static_cast<const InputMethodOperator *>(op);
        writer.writeTextElement(QLatin1String("string"), input->text());
        break;
    }
    case KLMOperator::Mental:
    case KLMOperator::Homing:
    case KLMOperator::WaitStart:
    case KLMOperator::WaitStop:
        break;
    }
    writer.writeEndElement();
}

bool KLMTableModel::readModel(QIODevice &readDevice)
{
    KLMMLReader reader;
    bool result = reader.read(&readDevice);
    if (result) {
        setTaskName(reader.taskName());
        operators = reader.ops();
        beginInsertRows(QModelIndex(), 0, operators.size() - 1);
        endInsertRows();
    } else {
        QString errorMessage;
        switch (reader.error()) {
        case QXmlStreamReader::NoError:
            errorMessage = tr("No error has occurred.");
            break;
        default:
        case QXmlStreamReader::CustomError:
            errorMessage = tr("Custom Error");
            break;
        case QXmlStreamReader::NotWellFormedError:
            errorMessage = tr("The parser internally raised an error due to the XML not being well-formed.");
            break;
        case QXmlStreamReader::PrematureEndOfDocumentError:
            errorMessage = tr("The input stream ended before the document was parsed completely.");
            break;
        case QXmlStreamReader::UnexpectedElementError:
            errorMessage = tr("The parser encountered an element that was different than what it expected.");
            break;
        }
        QMessageBox mbox(QMessageBox::Critical, tr("Unable to Load Model"),
                         tr("There was an error reading the KLM model"));
        mbox.setInformativeText(errorMessage);
        mbox.exec();
    }
    setDirty(false);
    return result;
}


void KLMTableModel::setTaskName(const QString &taskName)
{
    if (taskName == mTaskName)
        return;

    mTaskName = taskName;
    emit taskNameChanged(mTaskName);
    setDirty(true);
}

QString KLMTableModel::taskName() const
{
    return mTaskName;
}

bool KLMTableModel::isDirty() const
{
    return mDirty;
}

void KLMTableModel::updateDirtyStatus(bool clean)
{
    setDirty(!clean);
}

void KLMTableModel::setDirty(bool newStatus)
{
    if (mDirty == newStatus)
        return;

    mDirty = newStatus;
    emit dirtyStatusChanged(mDirty);
    if (!mDirty)
        mUndoStack->setClean();
}

struct InsertCommandInfo
{
    InsertCommandInfo(int r, KLMOperator *o) : row(r), op(o), timeDiff(0) {}
    int row;
    KLMOperator *op;
    int timeDiff;
};

class InsertOperatorCommand : public QUndoCommand
{
public:
    InsertOperatorCommand(KLMTableModel *model, int row, KLMOperator *op)
        : mModel(model)
    {
        commandOps.append(InsertCommandInfo(row, op));
        setText(KLMTableModel::tr("insert %1 operator").arg(operatorName(op)));
        refHash.insert(op, this);
    }
    ~InsertOperatorCommand()
    {
        for (int i = 0; i < commandOps.count(); ++i) {
            KLMOperator *op = commandOps.at(i).op;
            refHash.remove(op, this);
            if (mModel->rowOf(op) == -1) {
                // Op is no longer in the model, but other commands could have access to it.
                // Delete it if no one has it though.
                if (!refHash.contains(op)) {
                    delete op;
                }
            }
        }
    }
    int id() const {
        return 1000;
    }
    virtual void undo() {
        for (int i = 0; i < commandOps.count(); ++i) {
            InsertCommandInfo &info = commandOps[i];
            KLMOperator *mOp = info.op;
            int row = mModel->rowOf(mOp);
            mModel->beginRemoveRows(QModelIndex(), row, row);
            mModel->operators.removeAt(row);
            mModel->endRemoveRows();
            // We need to update the operator time now.
            if (row < mModel->rowCount() && info.timeDiff > 0) {
                for (int i = row; i < mModel->rowCount(); ++i) {
                    mModel->operators[i]->setOperatorTime(mModel->operators[i]->operatorTime().addMSecs(-info.timeDiff));
                }
                emit mModel->dataChanged(mModel->index(row, KLMTableModel::Time),
                                         mModel->index(mModel->rowCount() - 1, KLMTableModel::Time));
            }
        }
    }
    virtual void redo() {
        for (int i = 0; i < commandOps.count(); ++i) {
            InsertCommandInfo &info = commandOps[i];
            int mRow = info.row;
            KLMOperator *mOp = info.op;
            // OK, an insert. Here's how it works, inserted operators "associate" with the operator
            // before them. This means that you can fill gaps in time differences between two operators
            // by inserting new operators. This also means you can add things at the end with no real worries.
            // If the operator's time + duration is greater than the next operators start time, then
            // the differences is calculated and added to all all the following operators.

            // If the row is 0 (the begining), then that operator starts the whole thing and it must associate with the
            // next value.
            if (mModel->rowCount() != 0) {
                if (mRow < mModel->operators.size()) {
                    QTime nextTime = mOp->operatorTime().addMSecs(mOp->duration());
                    const KLMOperator *currentOp = mModel->operators.at(mRow);
                    if (nextTime > currentOp->operatorTime()) {
                        info.timeDiff = currentOp->operatorTime().msecsTo(nextTime);
                    }
                }
                if (mRow >= 0 && info.timeDiff > 0) {
                    for (int i = mRow; i < mModel->operators.size(); ++i) {
                        KLMOperator *oldOp = mModel->operators.at(i);
                        oldOp->setOperatorTime(oldOp->operatorTime().addMSecs(info.timeDiff));
                    }
                    emit mModel->dataChanged(mModel->index(0, KLMTableModel::Time),
                                             mModel->index(mRow - 1, KLMTableModel::Time));
                }
            }
            mModel->beginInsertRows(QModelIndex(), mRow, mRow);
            if (mRow == mModel->operators.size())
                mModel->operators.append(mOp);
            else
                mModel->operators.insert(mRow, mOp);
            mModel->endInsertRows();
        }
    }
protected:
    static QString operatorName(KLMOperator *op) {
        QString opName;
        switch (op->type()) {
        case KLMOperator::Mental:
            opName = KLMTableModel::tr("Mental");
            break;
        case KLMOperator::Point:
            opName = KLMTableModel::tr("Point");
            break;
        case KLMOperator::KeyPress:
        case KLMOperator::KeyRelease:
        case KLMOperator::ButtonPress:
        case KLMOperator::ButtonRelease:
            opName = KLMTableModel::tr("Key");
            break;
        case KLMOperator::Homing:
            opName = KLMTableModel::tr("Homing");
            break;
        case KLMOperator::IMComposing:
            opName = KLMTableModel::tr("Input Method Composing");
            break;
        case KLMOperator::IMCommit:
            opName = KLMTableModel::tr("Input Method Commit");
            break;
        case KLMOperator::WaitStart:
        case KLMOperator::WaitStop:
            opName = KLMTableModel::tr("Wait for Response");
            break;
        }
        return opName;
    }
    // refcounting hash for lazy people.
    static QMultiHash<KLMOperator *, InsertOperatorCommand *> refHash;
    mutable QList<InsertCommandInfo> commandOps;
    KLMTableModel *mModel;
};
QMultiHash<KLMOperator *, InsertOperatorCommand *> InsertOperatorCommand::refHash;
class RemoveOperatorCommand : public InsertOperatorCommand
{
public:
    RemoveOperatorCommand(KLMTableModel *model, int row, KLMOperator *op)
        : InsertOperatorCommand(model, row, op)
    {
        commandOps[0].timeDiff = op->duration();
        setText(KLMTableModel::tr("remove %1 operator").arg(operatorName(op)));
    }

    int id() const {
        return 1001;
    }

    virtual void undo() {
        InsertOperatorCommand::redo();
    }

    virtual void redo() {
        InsertOperatorCommand::undo();
    }
};

bool KLMTableModel::removeRows(int row, int count, const QModelIndex &)
{
    if (row < 0 || row >= operators.size()
        || row + count > operators.size())
        return false;

    for (int i = 0; i < count; ++i) {
        mUndoStack->push(new RemoveOperatorCommand(this, row, operators.at(row)));
    }
    return true;
}

void KLMTableModel::insertOperator(int row, KLMOperator *op)
{
    // hmm... we need to dial everything back...
    if (row > operators.size() || row < 0)
        return;
    InsertOperatorCommand *command = new InsertOperatorCommand(this, row, op);
    mUndoStack->push(command);
}

KLMOperator *KLMTableModel::operatorAt(const QModelIndex &index) const
{
    KLMOperator *op = 0;
    if (index.isValid())
        op = operators.at(index.row());
    return op;
}

Qt::ItemFlags KLMTableModel::flags(const QModelIndex &index) const
{
    if (!index.isValid())
        return 0;

    Qt::ItemFlags retFlags = (Qt::ItemIsSelectable | Qt::ItemIsEnabled);
    if (index.column() == Note)
        retFlags |= Qt::ItemIsEditable;
    return retFlags;
}

int KLMTableModel::rowOf(KLMOperator *op) const
{
    return operators.indexOf(op);
}

class SetNoteCommand : public QUndoCommand
{
public:
    SetNoteCommand(KLMTableModel *model, int row, const QString &note)
        : mModel(model), mRow(row), mNote(note)
    {
        setText(KLMTableModel::tr("set note"));
    }
    virtual void undo() {
        QModelIndex index = mModel->index(mRow, KLMTableModel::Note);
        KLMOperator *op = mModel->operatorAt(index);
        op->setNote(mOldNote);
        emit mModel->dataChanged(index, index);
    }
    virtual void redo() {
        QModelIndex index = mModel->index(mRow, KLMTableModel::Note);
        KLMOperator *op = mModel->operatorAt(index);
        Q_ASSERT(op);
        mOldNote = op->note();
        op->setNote(mNote);
        emit mModel->dataChanged(index, index);
    }
private:
    KLMTableModel *mModel;
    int mRow;
    QString mNote;
    QString mOldNote;
};

bool KLMTableModel::setData(const QModelIndex &index, const QVariant &value, int role)
{
    if (!index.isValid() || role != Qt::EditRole || index.column() != Note)
        return false;

    SetNoteCommand *command = new SetNoteCommand(this, index.row(), value.toString());
    mUndoStack->push(command);
    return true;
}

void KLMTableModel::guessMsByTime()
{
    static const int MinMentalTime = QSettings().value(QLatin1String("M-threshold"), 1200).toInt();
    for (int i = 1; i < operators.size(); ++i) {
        const KLMOperator *op1 = operators.at(i -1);
        const KLMOperator *op2= operators.at(i);
        if (op1->type() == KLMOperator::Mental || op2->type() == KLMOperator::Mental) {
            continue;
        }
        QTime endOpTime = op1->operatorTime().addMSecs(op1->duration());
        int msecsToOp2 = endOpTime.msecsTo(op2->operatorTime());
        if (msecsToOp2 >= MinMentalTime) {
            KLMOperator *mentalOperator = new KLMOperator(KLMOperator::Mental, endOpTime, msecsToOp2);
            mentalOperator->setNote(KLMTableModel::tr("Auto-computed M"));
            insertOperator(i, mentalOperator);
            ++i;
        }
    }
}

int KLMTableModel::timeForTask() const
{
    if (operators.isEmpty())
        return 0;

    KLMOperator *op = operators.at(0);
    QTime startTime = op->operatorTime();
    QTime computeTime = startTime;
    computeTime = computeTime.addMSecs(op->duration());
    for (int i = 1; i < operators.size(); ++i) {
        op = operators.at(i);
        int difference = computeTime.msecsTo(op->operatorTime()) + op->duration();
        computeTime = computeTime.addMSecs(difference);
    }
    return startTime.msecsTo(computeTime);

}
