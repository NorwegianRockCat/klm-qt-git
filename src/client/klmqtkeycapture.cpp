/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtGui/QKeyEvent>
#include "klmqtkeycapture.h"
#include "klmqtapp.h"

KLMQtKeyCapture::KLMQtKeyCapture(QWidget *parent)
    : QLabel(parent)
{
    setAlignment(Qt::AlignCenter);
    setFrameShape(QFrame::Box);
    setFocusPolicy(Qt::ClickFocus);
    setAttribute(Qt::WA_MacShowFocusRect);
    clearKey();
}

KLMQtKeyCapture::~KLMQtKeyCapture()
{
}

bool KLMQtKeyCapture::event(QEvent *ev)
{
    bool eaten = false;
    switch (ev->type()) {
    case QEvent::KeyPress:
        keyPressEvent(static_cast<QKeyEvent *>(ev));
        eaten = true;
        break;
    default:
        eaten = QLabel::event(ev);
    }
    return eaten;
}

void KLMQtKeyCapture::keyPressEvent(QKeyEvent *keyEvent)
{
    m_keyString = keyEvent->text();
    m_key = keyEvent->key();
    QString labelText = KLMQtApplication::keyToString(m_key);
    if (labelText.isEmpty())
        labelText = m_keyString;
    setText(labelText);
    clearFocus();
    emit keySelected(m_key);
}

void KLMQtKeyCapture::mousePressEvent(QMouseEvent *)
{
    setText(QString());
}

void KLMQtKeyCapture::clearKey()
{
    setText(tr("Type a key"));
    m_key = -1;
    m_keyString = QString();
}
