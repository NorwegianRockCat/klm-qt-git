/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#include <QtCore/QFile>
#include <QtGui/QFileDialog>
#include <QtGui/QHeaderView>
#include <QtGui/QPushButton>
#include <QtGui/QTableView>
#include <QtGui/QCloseEvent>
#include <QtGui/QMessageBox>
#include <QtCore/QDebug>
#include <QtGui/QPrintDialog>
#include <QtGui/QPrinter>
#include <QtGui/QPainter>
#include <QtGui/QTextDocument>
#include <QtGui/QTextFrame>
#include <QtGui/QTextTable>
#include <QtGui/QTextCursor>

#include "klmqtview.h"
#include "klmtablemodel.h"
#include "klmoperatordialog.h"
#include "klmqtapp.h"
#include "operator.h"

#if defined(Q_WS_MAC) && QT_VERSION < 0x040400
#include <Carbon/Carbon.h> // For Proxy Icons.
#endif

KLMQtView::KLMQtView(QWidget *parent)
    : QMainWindow(parent), mModel(0)
{
    setCentralWidget(new QWidget(this));
    setupUi(centralWidget());
    setWindowTitle(centralWidget()->windowTitle());
    setModel(new KLMTableModel());
    init();
}


KLMQtView::KLMQtView(KLMTableModel *model, QWidget *parent)
    : QMainWindow(parent), mModel(0)
{
    setCentralWidget(new QWidget(this));
    setupUi(centralWidget());
    setWindowTitle(centralWidget()->windowTitle());
    setModel(model);
    init();
}

void KLMQtView::init()
{
    totalTimeLabel->setMinimumWidth(QFontMetrics(totalTimeLabel->font())
                                                .width(QLatin1String("WWWWWW")));
    tableView->setModel(mModel);
    tableView->horizontalHeader()->setResizeMode(QHeaderView::Interactive);
    tableView->horizontalHeader()->setStretchLastSection(true);
    tableView->verticalHeader()->hide();
    connect(taskNameLineEdit, SIGNAL(textEdited(const QString &)),
            this, SLOT(on_taskNameLineEdit_textEdited(const QString &)));
    connect(removeOperatorButton, SIGNAL(clicked()),
            this, SLOT(on_removeOperatorButton_clicked()));
    connect(addOperatorBeforeButton, SIGNAL(clicked()),
            this, SLOT(on_addOperatorBeforeButton_clicked()));
    connect(addOperatorAfterButton, SIGNAL(clicked()),
            this, SLOT(on_addOperatorAfterButton_clicked()));
    connect(addNoteButton, SIGNAL(clicked()),
            this, SLOT(on_addNoteButton_clicked()));
    connect(guessMButton, SIGNAL(clicked()),
            this, SLOT(on_guessMButton_clicked()));
    connect(mModel, SIGNAL(taskNameChanged(const QString &)),
            this, SLOT(updateTaskName(const QString &)));
    connect(mModel, SIGNAL(dirtyStatusChanged(bool)),
            this, SLOT(setWindowModified(bool)));
    connect(tableView->selectionModel(),
            SIGNAL(selectionChanged(const QItemSelection &, const QItemSelection)),
            this, SLOT(updateButtons()));
    connect(mModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SLOT(updateButtons()));
    connect(mModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SLOT(updateButtons()));
    connect(mModel, SIGNAL(rowsInserted(const QModelIndex &, int, int)),
            this, SLOT(calcTotalTime()));
    connect(mModel, SIGNAL(rowsRemoved(const QModelIndex &, int, int)),
            this, SLOT(calcTotalTime()));
    connect(this, SIGNAL(addAvailable(bool)), addOperatorAfterButton, SLOT(setEnabled(bool)));
    connect(this, SIGNAL(addAvailable(bool)), addOperatorBeforeButton, SLOT(setEnabled(bool)));
    connect(this, SIGNAL(removeAvailable(bool)), removeOperatorButton, SLOT(setEnabled(bool)));
    connect(this, SIGNAL(addNoteAvailable(bool)), addNoteButton, SLOT(setEnabled(bool)));
    connect(this, SIGNAL(guessMAvailable(bool)), guessMButton, SLOT(setEnabled(bool)));
    updateButtons();
}

KLMQtView::~KLMQtView()
{
}


void KLMQtView::setModel(KLMTableModel *model)
{
    delete mModel;
    mModel = model;
    mModel->setParent(this);
    updateTaskName(mModel->taskName());
    if (taskNameLineEdit->text().isEmpty())
        taskNameLineEdit->setText(QLatin1String("Unamed Task"));
    setWindowModified(mModel->isDirty());
    calcTotalTime();
}

KLMTableModel *KLMQtView::model() const
{
    return mModel;
}

void KLMQtView::closeEvent(QCloseEvent *event)
{
    if (mModel->isDirty()) {
        QMessageBox messageBox(QMessageBox::Information, tr("KLM Client"),
           tr("Do you want to save your changes to this model before closing?"),
           QMessageBox::Discard | QMessageBox::Save | QMessageBox::Cancel,
           this);
        messageBox.setInformativeText(tr("If you don't save, your changes will be lost."));
        messageBox.setWindowModality(Qt::WindowModal);
        messageBox.setDefaultButton(static_cast<QPushButton *>(messageBox.button(QMessageBox::Save)));
        switch (messageBox.exec()) {
        case QMessageBox::Discard:
            mModel->setDirty(false);
            setWindowModified(false);
            break;
        case QMessageBox::Save:
            saveModel();
            break;
        case QMessageBox::Cancel:
            event->ignore();
            return;
        }
    }
    QMainWindow::closeEvent(event);
}

bool KLMQtView::saveModelAs()
{
    QString saveName = QFileDialog::getSaveFileName(this);
    if (saveName.isEmpty())
        return false;

    return saveModel(saveName);
}

bool KLMQtView::saveModel()
{
    if (mFileName.isEmpty()) {
        return saveModelAs();
    }
   return saveModel(mFileName);
}

bool KLMQtView::saveModel(const QString &saveName)
{
    QFile file(saveName);
    if (!file.open(QIODevice::WriteOnly)) {
        QMessageBox messageBox(QMessageBox::Critical, tr("KLM Client"),
                               tr("Unable to open file for writing"),
                               QMessageBox::Retry | QMessageBox::Cancel
                               | QMessageBox::Abort,
                               this);
        messageBox.setWindowModality(Qt::WindowModal);
        messageBox.button(QMessageBox::Abort)->setText(tr("Change File Name..."));
        messageBox.setDefaultButton(static_cast<QPushButton *>(messageBox.button(QMessageBox::Retry)));
        bool fileOK = false;
        while (!fileOK) {
            messageBox.setInformativeText(tr("The operating system reported "
                        "this problem: %1. You can try to open the file again,"
                        " pick a new file, or cancel the operation.")
                                            .arg(file.errorString()));
            switch (messageBox.exec()) {
            case QMessageBox::Retry:
                fileOK = file.open(QIODevice::WriteOnly);
                break;
            case QMessageBox::Cancel:
                return false;
            case QMessageBox::Abort:
                saveModelAs();
                return false;
            }
        }
    }
    bool saveOK = mModel->saveModel(file);
    if (!saveOK) {
        QMessageBox messageBox(QMessageBox::Critical, tr("KLM Client"),
                               tr("Unable to write file"),
                               QMessageBox::Retry | QMessageBox::Cancel
                               | QMessageBox::Abort,
                               this);
        messageBox.setWindowModality(Qt::WindowModal);
        messageBox.button(QMessageBox::Abort)->setText(tr("Change File Name..."));
        messageBox.setDefaultButton(static_cast<QPushButton *>(messageBox.button(QMessageBox::Retry)));
        messageBox.setInformativeText(tr("The operating system reported "
                    "this problem: %1. You can try to save the file again,"
                    " pick a new file, or cancel the operation.")
                                        .arg(file.errorString()));
        switch (messageBox.exec()) {
        case QMessageBox::Retry:
            file.close();
            return saveModel(saveName);
        case QMessageBox::Cancel:
            return false;
        case QMessageBox::Abort:
            return saveModelAs();
        }
    }
    updateWindowTitle(saveName);
    return true;
}

void KLMQtView::on_taskNameLineEdit_textEdited(const QString &taskName)
{
    mModel->setTaskName(taskName);
}

void KLMQtView::updateWindowTitle(const QString &fileName)
{
    mFileName = fileName;
    setWindowTitle(QString::fromUtf8("%1—%2[*]").arg(QFileInfo(mFileName).fileName())
                                  .arg(taskNameLineEdit->text()));
#ifdef Q_WS_MAC
# if QT_VERSION >= 0x040400
    setWindowFilePath(fileName);
# else
    FSRef fsref;
    if (FSPathMakeRef(reinterpret_cast<const UInt8*>(fileName.toUtf8().constData()),
                      &fsref, 0) == noErr) {
        HIWindowSetProxyFSRef(HIViewGetWindow(HIViewRef(winId())), &fsref);
    }
# endif
#endif
}

void KLMQtView::updateTaskName(const QString &name)
{
    if (taskNameLineEdit->text() == name)
        return;

    taskNameLineEdit->setText(name);
}


void KLMQtView::setFileName(const QString &fileName)
{
    mFileName = fileName;
    updateWindowTitle(mFileName);
}

QString KLMQtView::fileName() const
{
    return mFileName;
}


void KLMQtView::raiseAndActivateWindow()
{
    raise();
    activateWindow();
}

void KLMQtView::on_removeOperatorButton_clicked()
{
    if (QItemSelectionModel *selectModel = tableView->selectionModel()) {
        int minRow = mModel->rowCount();
        int maxRow = -1;
        while (!selectModel->selectedRows().isEmpty()) {
            QModelIndex indexToDelete = selectModel->selectedRows().front();
            minRow = qMin(indexToDelete.row() - 1, minRow);
            maxRow = qMax(indexToDelete.row(), maxRow);
            selectModel->select(indexToDelete,
                                QItemSelectionModel::Deselect | QItemSelectionModel::Rows);
            mModel->removeRows(indexToDelete.row(), 1);
        }
        // Now that the selection is finished, let's see if we can select another row,
        // first we go for the above and then for the row below.
        int rowToSelect = -1;
        if (minRow >= 0 && minRow < mModel->rowCount())
            rowToSelect = minRow;
        else if (maxRow >= 0 && maxRow < mModel->rowCount())
            rowToSelect = maxRow;
        
        if (rowToSelect >= 0) {
            QModelIndex selectIndex = mModel->index(rowToSelect, 0);
            selectModel->select(selectIndex,
                                QItemSelectionModel::Select | QItemSelectionModel::Rows);
            tableView->scrollTo(selectIndex);
        }
        
        // else nothing to select.
    }
}

void KLMQtView::on_addOperatorBeforeButton_clicked()
{
    int startTime = 0;
    QTime baseTime = QTime::currentTime();
    int minRow = mModel->rowCount();
    if (mModel->rowCount() > 0) {
        baseTime = mModel->operatorAt(mModel->index(0, 0))->operatorTime();
    }
    if (QItemSelectionModel *selectModel = tableView->selectionModel()) {
        if (!selectModel->selectedRows().isEmpty()) {
            foreach (QModelIndex index, selectModel->selectedRows()) {
                minRow = qMin(index.row(), minRow);
            }
        }
        if (minRow > 0 && minRow < mModel->rowCount()) {
            KLMOperator *op = mModel->operatorAt(mModel->index(minRow - 1, 0));
            startTime = baseTime.msecsTo(op->operatorTime().addMSecs(op->duration()));
        }
    }
    KLMOperatorDialog *dialog = new KLMOperatorDialog(minRow, baseTime, startTime, this);
    connect(dialog, SIGNAL(operatorFinished(int, KLMOperator*)),
            this, SLOT(insertOperator(int, KLMOperator*)));
    dialog->show();

}

void KLMQtView::on_addOperatorAfterButton_clicked()
{
    int startTime = 0;
    int maxRow = -1;
    QTime baseTime = QTime::currentTime();
    if (mModel->rowCount() > 0) {
        baseTime = mModel->operatorAt(mModel->index(0, 0))->operatorTime();
    }
    if (QItemSelectionModel *selectModel = tableView->selectionModel()) {
        if (!selectModel->selectedRows().isEmpty()) {
            foreach (QModelIndex index, selectModel->selectedRows()) {
                maxRow = qMax(index.row(), maxRow);
            }
        }
        if (maxRow >= 0 && maxRow < mModel->rowCount()) {
            KLMOperator *op = mModel->operatorAt(mModel->index(maxRow, 0));
            startTime = baseTime.msecsTo(op->operatorTime().addMSecs(op->duration()));
        }
    }
    KLMOperatorDialog *dialog = new KLMOperatorDialog(
                                      (maxRow >= 0 && maxRow < mModel->rowCount()) ? maxRow + 1
                                                                                : mModel->rowCount(),
                                                      baseTime, startTime, this);
    connect(dialog, SIGNAL(operatorFinished(int, KLMOperator*)),
            this, SLOT(insertOperator(int, KLMOperator*)));
    dialog->show();
}

void KLMQtView::insertOperator(int row, KLMOperator *op)
{
    mModel->insertOperator(row, op);
    if (QItemSelectionModel *selectModel = tableView->selectionModel()) {
        selectModel->clear();
        QModelIndex selectIndex = mModel->index(row, 0);
        selectModel->select(selectIndex,
                            QItemSelectionModel::Select | QItemSelectionModel::Rows);
        tableView->scrollTo(selectIndex);
    }
}

void KLMQtView::on_addNoteButton_clicked()
{
    QModelIndexList rows = tableView->selectionModel()->selectedRows(4);
    if (!rows.isEmpty()) {
        QModelIndex index = rows.first();
        tableView->setCurrentIndex(index);
        tableView->edit(index);
    }

}

void KLMQtView::on_guessMButton_clicked()
{
    mModel->guessMsByTime();
}

void KLMQtView::updateButtons()
{
    QModelIndexList indexes = tableView->selectionModel()->selectedIndexes();
    bool removeAvail;
    bool addAvail;
    bool noteAvail;
    bool guessMAvail;
    if (indexes.isEmpty()) {
        removeAvail = false;
        noteAvail = false;
        if (mModel->rowCount() == 0) {
            addAvail = true;
            guessMAvail = false;
        } else {
            addAvail = false;
            guessMAvail = true;
        }
    } else {
        guessMAvail = true;
        addAvail = true;
        removeAvail = true;
        noteAvail = true;
        int currRow = indexes.at(0).row();
        for (int i = 1; i < indexes.size(); ++i) {
            if (indexes.at(i).row() != currRow) {
                noteAvail = false;
                break;
            }
        }
    }
    emit addAvailable(addAvail);
    emit removeAvailable(removeAvail);
    emit addNoteAvailable(noteAvail);
    emit guessMAvailable(guessMAvail);
}

void KLMQtView::printModel()
{
    QPrinter *printer = KLMQtApplication::printer();
    QPrintDialog printDialog(printer, this);
    if (printDialog.exec() == QDialog::Accepted)
        printModel(printer);
}

void KLMQtView::printModel(QPrinter *printer)
{
    QTextDocument document;
    document.setDefaultStyleSheet(QLatin1String("table: border-collapse: collapse;"));
    QFont font("times");
    font.setPointSize(11);
    document.setDefaultFont(font);
    document.setUndoRedoEnabled(false);
    document.setMetaInformation(QTextDocument::DocumentTitle, mModel->taskName());
    QTextFrame *root = document.rootFrame();
    QTextFrameFormat frameFormat = root->frameFormat();
    frameFormat.setBorderStyle(QTextFrameFormat::BorderStyle_DotDash);
    root->setFrameFormat(frameFormat);
    QTextCursor tc = root->firstCursorPosition();

    if (!mModel->taskName().isEmpty()) {
        tc.insertHtml(tr("<b>Task Name</b>: %1<br><br>")
                       .arg(mModel->taskName()));
    }

    QTextTable *table = tc.insertTable(mModel->rowCount() + 1, mModel->columnCount());
    QTextTableFormat format = table->format();
    format.setCellSpacing(9);
    format.setBorderStyle(QTextFrameFormat::BorderStyle_None);
    format.setAlignment(Qt::AlignCenter);
    format.setHeaderRowCount(1);
    table->setFormat(format);
    // the header
    for (int col = 0; col < mModel->columnCount(); ++col) {
        QTextTableCell tableCell = table->cellAt(0, col);
        QTextCharFormat charFormat = tableCell.format();
        charFormat.setFontWeight(75);
        tableCell.setFormat(charFormat);
        QTextCursor cursor = tableCell.firstCursorPosition();
        QTextBlockFormat blockFormat = cursor.blockFormat();
        blockFormat.setAlignment(Qt::AlignCenter);
        cursor.setBlockFormat(blockFormat);
        cursor.insertText(mModel->headerData(col, Qt::Horizontal).toString());
    }

    // Now the data.
    for (int row = 0; row < mModel->rowCount(); ++row) {
        int tableRow = row + 1;
        for (int col = 0; col < mModel->columnCount(); ++col) {
            QTextTableCell tableCell = table->cellAt(tableRow, col);
            QTextCursor cursor = tableCell.firstCursorPosition();
            QTextBlockFormat blockFormat = cursor.blockFormat();
            if (col == 0 || col == 3 || col == 4)
                blockFormat.setAlignment(Qt::AlignCenter);
            else
                blockFormat.setAlignment(Qt::AlignRight|Qt::AlignVCenter);
            cursor.setBlockFormat(blockFormat);
            cursor.insertText(mModel->data(mModel->index(row, col)).toString());
        }
    }
    document.print(printer);
}

void KLMQtView::calcTotalTime()
{
    totalTimeLabel->setNum(mModel->timeForTask());
}
