/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef GOMSCONTROLLER_H
#define GOMSCONTROLLER_H

#include <QtGui/QMainWindow>
#include <QtCore/QDateTime>
#include "ui_klmqtcontroller.h"
#include "listenerclient.h"

class QAction;
class QActionGroup;
class QCloseEvent;
class QMenuBar;
class QTimer;
class QUndoGroup;
class BonjourServiceResolver;
class KLMQtView;
class KLMTableModel;
class PreferenceWindow;
class QMenuBar;

class KLMQtController : public QMainWindow, private Ui::KLMQtController
{
    Q_OBJECT
public:
    KLMQtController();
    ~KLMQtController();
    bool handleClose();

public slots:
    void on_cmdStartRecording_clicked();
    void recordingChanged(ListenerClient::RecordingStatus);
    void raiseAndActivateWindow();
    bool openModel(const QString &fileName);

protected:
    void closeEvent(QCloseEvent *event);

private slots:
    void updateTime();
    void saveModelAs();
    void newModel();
    void openModel();
    void saveModel();
    void closeModel();
    void updateServerCombo(const QList<BonjourRecord> &list);
    void minimizeCurrentWindow();
    void zoomCurrentWindow();
    void generateWindowList();
    void updateMenu(QWidget *, QWidget *newWidget);
    void bringAllToFront();
    void removeViewFromList();
    void handleQuit();
    void updateUndoStack(KLMQtView *activeView);
    void openRecentModel();
    void clearRecentFiles();
    void showPreferences();
    void cleanupPreferences();
    void deleteOperator();
    void addOperatorBefore();
    void addOperatorAfter();
    void editNote();
    void computeMs();
    void printModel();
    void pageSetup();

private:
    enum { MaxRecentFiles = 10 };
    KLMQtView *activeKLMView() const;
    void clearTimeLabel();
    void setupMenu(QMenuBar *menubar);
    void updateRecentFileActions();
    void addRecentFile(const QString &fileName);
    void addActionsToMenu(QMenu *menu, QList<QAction *> actions);
    KLMQtView *createKLMView(KLMTableModel *model = 0);
    QList<KLMQtView *> mList;
    ListenerClient *mListener;
    ListenerClient::RecordingStatus recordingStatus;
    QTimer *mTimer;
    QTime curTime;
    QAction *minimizedAction;
    QAction *zoomAction;
    QAction *bringToFrontAction;
    QAction *saveAction;
    QAction *saveAsAction;
    QAction *closeAction;
    QAction *removeOpAction;
    QAction *addOpBeforeAction;
    QAction *addOpAfterAction;
    QAction *addNoteAction;
    QAction *guessMAction;
    QAction *printAction;
    QAction *pageSetupAction;
    BonjourServiceResolver *serviceResolver;
    bool mReadyToQuit;
    QUndoGroup *mUndoGroup;
    QActionGroup *mRecentFilesActions;
    PreferenceWindow *mPrefWindow;
    KLMQtView *oldView;
};

#endif // GOMSCONTROLLER_H
