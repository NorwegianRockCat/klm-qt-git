/*
 * Copyright (c) 2006-2008, Trenton W. Schulz
 *
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this
 * list of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of the University of Oslo nor the names of its contributors
 * may be used to endorse or promote products derived from this software without
 * specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
 * LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR
 * A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR
 * CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL,
 * EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO,
 * PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR
 * PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF
 * LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING
 * NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS
 * SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */
#ifndef LISTENERCLIENT_H
#define LISTENERCLIENT_H

#include <QtCore/QList>
#include <QtCore/QObject>
#include <QtNetwork/QAbstractSocket>
#include <dns_sd.h>

#include "bonjourrecord.h"

class QTcpSocket;
class QSocketNotifier;
class QHostInfo;
class KLMOperator;

class ListenerClient : public QObject
{
    Q_OBJECT
public:
    enum SocketState { Disconnected, WaitingForConnect, Connected, StartReceiving, ReceivingData, Error };
    enum RecordingStatus { NotRecording, Waiting, Recording };
    ListenerClient(QObject *parent);
    ~ListenerClient();
    QList<KLMOperator *> klmOperators() const { return klmOps; }
    void clear();
    RecordingStatus recordingStatus() const { return mRecordingStatus; }
    void tryDisconnect();

public slots:
    void stopRecording();
    void connectToServer(const QHostInfo &hostInfo, int port);

signals:
    void recordingStatusChanged(ListenerClient::RecordingStatus recordingStatus);

private slots:
    void readCommand();
    void connectedToHost();
    void disconnectedFromHost();
    void socketError(QAbstractSocket::SocketError error);
    void errorState();

private:
    void updateRecordingStatus(RecordingStatus newStatus);
    void processCommand(const QByteArray &cmd);
    void startRecording_helper();
    void stopRecording_helper();
    void fetchOperator();
    void sayBye();
    void decodeOperator(const QByteArray &block);

    QList<KLMOperator *> klmOps;
    QTcpSocket *mSocket;
    SocketState status;
    RecordingStatus mRecordingStatus;
    int dataSize;
};

#endif // LISTENERCLIENT_H
