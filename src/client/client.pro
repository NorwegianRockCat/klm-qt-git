QT += network xml

HEADERS += klmtablemodel.h \
           listenerclient.h \
           klmqtcontroller.h \
           klmqtview.h \
           ../shared/bonjourrecord.h \
           bonjourservicebrowser.h \
           bonjourserviceresolver.h \
           bonjourtreewidget.h \
           klmqtapp.h \
           klmoperatordialog.h \
           faderwidget.h \
           klmqtkeycapture.h \
           klmmlreader.h \
           preferencewindow.h


SOURCES += klmtablemodel.cpp \
           listenerclient.cpp \
           klmqtcontroller.cpp \
           klmqtview.cpp main.cpp \
           bonjourtreewidget.cpp \
           bonjourservicebrowser.cpp \
           bonjourserviceresolver.cpp \
           klmqtapp.cpp \
           klmoperatordialog.cpp \
           faderwidget.cpp \
           klmqtkeycapture.cpp \
           klmmlreader.cpp \
           preferencewindow.cpp

FORMS += klmqtview.ui \
         klmqtcontroller.ui \
         klmoperatordialog.ui \
         preferencewindow.ui

INCLUDEPATH += ../shared
DESTDIR = ../../bin
mac {
    TARGET = KLMQt
} else {
    TARGET = klmqt
}

PRECOMPILED_HEADER = client_pch.h
CONFIG += no_objective_c

!mac:!windows: LIBS += -ldns_sd

windows: LIBS += -ldnssd

mac {
    ICON=images/klmqt.icns
    QMAKE_INFO_PLIST=Info.plist
    EXTRA_ICONS.files = images/klmdoc.icns
    EXTRA_ICONS.path = Contents/Resources
    QMAKE_BUNDLE_DATA += EXTRA_ICONS
}
