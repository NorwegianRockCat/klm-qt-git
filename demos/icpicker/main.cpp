#include <QtGui/QApplication>
#include "icpicker.h"
#include "listener.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    ICPicker widget;
    Listener listener(QAbstractEventDispatcher::instance());

    widget.show();

    return app.exec();
}
