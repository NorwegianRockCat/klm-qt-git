#include "icpicker.h"

ICPicker::ICPicker(QWidget *parent)
    : QDialog(parent)
{
    setupUi(this);
}

void ICPicker::showEvent(QShowEvent *event)
{
    lineEdit->setFocus();
    QDialog::showEvent(event);
}

void ICPicker::done(int ret)
{
    QDialog::done(ret);
}
