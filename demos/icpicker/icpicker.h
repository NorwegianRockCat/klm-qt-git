#ifndef ICPICKER_H
#define ICPICKER_H

#include "ui_icecream-picker.h"

class ICPicker : public QDialog, private Ui::ICPicker
{
    Q_OBJECT
public:
    ICPicker(QWidget *parent = 0);

public slots:
    void done(int ret);

protected:
    void showEvent(QShowEvent *show);
};

#endif // ICPICKER_H
