#ifndef TEMPERATUREWINDOWV2_H
#define TEMPERATUREWINDOWV2_H

#include <QtGui/QWidget>
#include "ui_temperaturewindowv2.h"

class TemperatureWindowV2 : public QWidget, public Ui::TemperatureWindowV2
{
    Q_OBJECT
public:
    TemperatureWindowV2(QWidget *parent = 0);
    ~TemperatureWindowV2() {}

private slots:
    void computeFahrenheit(int value);
    void computeCelsius(int value);
};

#endif //TEMPERATUREWINDOWV2_H
