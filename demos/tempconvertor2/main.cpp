#include <QtGui/QApplication>
#include "temperaturewindowv2.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    TemperatureWindowV2 window;
    window.show();
    return app.exec();
}
