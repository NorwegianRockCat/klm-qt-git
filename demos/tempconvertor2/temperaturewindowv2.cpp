#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QFontMetrics>

#include "temperaturewindowV2.h"

TemperatureWindowV2::TemperatureWindowV2(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    connect(sliderCelsius, SIGNAL(valueChanged(int)), this, SLOT(computeFahrenheit(int)));
    connect(sliderFahrenheit, SIGNAL(valueChanged(int)), this, SLOT(computeCelsius(int)));
    resultFahrenheit->setMinimumWidth(QFontMetrics(resultFahrenheit->font()).width("WWW.WWWWWW"));
    resultCelsius->setMinimumWidth(QFontMetrics(resultCelsius->font()).width("WWW.WWWWWW"));
    computeCelsius(sliderCelsius->value());
    computeFahrenheit(sliderFahrenheit->value());
}

void TemperatureWindowV2::computeFahrenheit(int value)
{
    qreal originalTemp = value / 100.0;
    qreal finalTemp = (9.0 / 5) * originalTemp + 32;
    resultFahrenheit->setNum(finalTemp);
    sliderFahrenheit->setValue(finalTemp * 100);
}

void TemperatureWindowV2::computeCelsius(int value)
{
    qreal originalTemp = value / 100.0;
    qreal finalTemp = (originalTemp - 32) * (5.0 / 9);
    resultCelsius->setNum(finalTemp);
    sliderCelsius->setValue(finalTemp * 100);
}
