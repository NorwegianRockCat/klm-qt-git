#ifndef TEMPERATUREWINDOW_H
#define TEMPERATUREWINDOW_H

#include <QtGui/QWidget>
#include "ui_currencywindow.h"

class CurrencyWindow : public QWidget, public Ui::CurrencyWindow
{
    Q_OBJECT
public:
    CurrencyWindow(QWidget *parent = 0);
    ~CurrencyWindow() {}

private slots:
    void computeTemperature();
};

#endif //TEMPERATUREWINDOW_H
