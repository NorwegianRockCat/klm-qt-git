#include <QtGui/QApplication>
#include "currencywindow.h"
#include "listener.h"

int main(int argc, char **argv)
{
    QApplication app(argc, argv);
    CurrencyWindow window;
    Listener listener(QAbstractEventDispatcher::instance());
    QObject::connect(&listener, SIGNAL(recordingStatusChanged(bool)), &window, SLOT(raise()));
#ifdef Q_WS_QWS
    window.showMaximized();
#else
    window.show();
#endif
    return app.exec();
}
