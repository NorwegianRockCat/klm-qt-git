#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QFontMetrics>

#include "currencywindow.h"

CurrencyWindow::CurrencyWindow(QWidget *parent)
    : QWidget(parent)
{
    setupUi(this);
    connect(temperatureEdit, SIGNAL(returnPressed()), this, SLOT(computeTemperature()));
    resultLabel->setMinimumWidth(QFontMetrics(resultLabel->font()).width("WWW.WWWWWW"));
    resultLabel->setText("0.00");
}

void CurrencyWindow::computeTemperature()
{
    qreal originalTemp = qreal(temperatureEdit->text().toDouble());
    qreal finalTemp;
    if (radioCelsius->isChecked()) {
        finalTemp = originalTemp * 0.19;
    } else if (radioFahrenheit->isChecked()) {
        finalTemp = originalTemp * 5.35;
    }
    resultLabel->setNum(finalTemp);
}
