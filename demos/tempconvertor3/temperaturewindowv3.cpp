#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QRadioButton>
#include <QtGui/QFontMetrics>
#include <QtGui/QValidator>

#include "temperaturewindowv3.h"

TemperatureWindowV3::TemperatureWindowV3(QWidget *parent, Qt::WFlags flags)
    : QWidget(parent, flags)
{
    setupUi(this);
    connect(lineEdit, SIGNAL(textChanged(QString)),
            this, SLOT(computeTemperature(QString)));
    fahrenheitLabel->setMinimumWidth(QFontMetrics(fahrenheitLabel->font()).width("WWW.WWW"));
    celsiusLabel->setMinimumWidth(QFontMetrics(celsiusLabel->font()).width("WWW.WWW"));
    computeTemperature(QString());
    lineEdit->setValidator(new QIntValidator(lineEdit));
}

void TemperatureWindowV3::computeTemperature(const QString &text)
{
    bool ok;
    qreal originalTemp = qreal(text.toDouble(&ok));
    if (!ok) {
        fahrenheitLabel->clear();
        celsiusLabel->clear();
    } else {
        qreal finalTemp = (9.0 / 5) * originalTemp + 32;
        fahrenheitLabel->setNum(finalTemp);
        finalTemp = (originalTemp - 32) * (5.0 / 9);
        celsiusLabel->setNum(finalTemp);
    }
}

