#ifndef TEMPERATUREWINDOWV3_H
#define TEMPERATUREWINDOWV3_H

#include <QtGui/QWidget>
#include "ui_temperaturewindowv3.h"

class TemperatureWindowV3 : public QWidget, public Ui::TemperatureWindowV3
{
    Q_OBJECT
public:
    TemperatureWindowV3(QWidget *parent = 0, Qt::WFlags = 0);
    ~TemperatureWindowV3() {}

private slots:
    void computeTemperature(const QString &text);
};

#endif //TEMPERATUREWINDOWV3_H
