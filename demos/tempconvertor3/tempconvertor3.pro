#qtopia_project(qtopia app)
#TARGET=tempconvertor
#CONFIG+=qtopia_main
#CONFIG+=no_singleexec
#CONFIG+=no_singleexec
#CONFIG+=no_quicklaunch
#CONFIG+=no_tr

# Input
FORMS += temperaturewindowv3.ui
HEADERS += temperaturewindowv3.h
SOURCES += main.cpp temperaturewindowv3.cpp
RESOURCES += tempconvertor.qrc

#desktop.files=tempconvertor.desktop
#desktop.path=/apps/Applications
#desktop.trtarget=tempconvertor-nct
#desktop.hint=nct desktop
#
#pics.files=pics/*
#pics.path=/pics/example
#pics.hint=pics
#
#help.source=help
#help.files=example.html
#help.hint=help
#
#INSTALLS+=desktop pics help

#pkg.name=tempconvertor
#pkg.desc=Temperature Convertor
#pkg.version=1.0.0-1
#pkg.maintainer=Trenton Schulz
#pkg.license=GPL
#pkg.domain=window

INCLUDEPATH += ../../include

LIBS += -L../../lib -lklmgenerator

!mac: LIBS += -ldns_sd
QT += network
