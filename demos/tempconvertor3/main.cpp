#include "temperaturewindowv3.h"
#include "listener.h"
//#include <qtopia/qtopiaapplication.h>
#include "qapplication.h"

//QTOPIA_ADD_APPLICATION(QTOPIA_TARGET, TemperatureWindowV3)

int main(int argc, char **argv)
{
    Q_INIT_RESOURCE(tempconvertor);

//    QtopiaApplication app(argc, argv);
    QApplication app(argc, argv);
    TemperatureWindowV3 *window = new TemperatureWindowV3;
    Listener listener(QAbstractEventDispatcher::instance());
    //app.setMainWidget(window);
    //app.showMainWidget();
    window->show();
    return app.exec();
}
